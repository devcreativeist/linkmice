// JavaScript Document
$(document).ready(function (e) {
	
			$('.mainMenu a').click(function () {
                $('.mainMenu a').removeClass('active');
				$(this).addClass('active');
				
            });
			

			$('.fancybox').fancybox();
			$(".various").fancybox({
				maxWidth: 800,
				maxHeight: 500,
				fitToView: false,
				width: '70%',
				height: '70%',
				autoSize: false,
				closeClick: false,
				openEffect: 'none',
				closeEffect: 'none',
	
			});
			$('.fancybox-media').fancybox({
				openEffect: 'none',
				closeEffect: 'none',
				height: 510,
				helpers: {
					media: {}
				}
			});
			
			$('.topContainer a[href*=#]:not([href=#])').click(function() {
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				  var target = $(this.hash);
				  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				  if (target.length) {
					$('html,body').animate({
					  scrollTop: target.offset().top - 131
					}, 1000);
					return false;
				  }
				}
			  });

		$("a[rel=example_group]").fancybox({
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'titlePosition' 	: 'over',
		'titleFormat'       : function(title, currentArray, currentIndex, currentOpts) {
		    return '<span id="fancybox-title-over">Image ' +  (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
		}
	});

	
	$(".various").fancybox({
		'transitionIn'	: 'none',
		'transitionOut'	: 'none'
	});
	
	
        });