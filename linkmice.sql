-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 16 Ara 2014, 16:43:45
-- Sunucu sürümü: 5.6.14
-- PHP Sürümü: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Veritabanı: `linkmice`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `anasayfa`
--

CREATE TABLE IF NOT EXISTS `anasayfa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isim` varchar(256) NOT NULL,
  `mekan` varchar(256) NOT NULL,
  `aciklama` text NOT NULL,
  `url` varchar(256) NOT NULL,
  `kategori` varchar(128) NOT NULL,
  `resim` varchar(256) NOT NULL,
  `jum` int(11) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `ekip`
--

CREATE TABLE IF NOT EXISTS `ekip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t01` varchar(256) NOT NULL,
  `t02` varchar(256) NOT NULL,
  `t03` varchar(256) NOT NULL,
  `t04` varchar(256) NOT NULL,
  `t05` varchar(256) NOT NULL,
  `jum` int(11) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Tablo döküm verisi `ekip`
--

INSERT INTO `ekip` (`id`, `t01`, `t02`, `t03`, `t04`, `t05`, `jum`, `statu`) VALUES
(1, 'Ali Arslan', 'Kurucu ortak / Şirket Müdürü ', 'Eskişehir Ünv. İşletme mezunu', '1972 İstanbul doğumlu. 18 yıl İnşaat ve dekorasyon sektör deneyimli.', '12.jpg', 0, 0),
(2, 'E. Mine Şimşek', 'Kurucu ortak / Koordinatör', 'Moda tasarım mezunu ', '1972 Rize doğumlu. 15 yıl çeşitli inşaat firmalarında dekorasyon deneyimli. ', '22.jpg', 0, 0),
(3, 'Dilek Yıldırım', 'Finans işleri / Muhasebe müdürü ', '', '1976 Ankara doğumlu. 15 yıl çeşitli firmalarda muhasebe deneyimli.', '13.jpg', 0, 0),
(10, 'Yiğit Baydar', 'Proje Grubu / Müşteri Temsilcisi ', 'Yeditepe Ünv. Sanat yönetimi mezunu ', '', '23.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `galeri`
--

CREATE TABLE IF NOT EXISTS `galeri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `t01` varchar(256) NOT NULL,
  `t02` varchar(256) NOT NULL,
  `t03` text NOT NULL,
  `t04` varchar(256) NOT NULL,
  `t05` varchar(256) NOT NULL,
  `seo` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `keys` varchar(256) NOT NULL,
  `desc` varchar(256) NOT NULL,
  `jum` int(11) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Tablo döküm verisi `galeri`
--

INSERT INTO `galeri` (`id`, `aid`, `t01`, `t02`, `t03`, `t04`, `t05`, `seo`, `title`, `keys`, `desc`, `jum`, `statu`) VALUES
(1, 0, 'Photo Gallery', '', '', '', '', '', '', '', '', 0, 0),
(2, 0, 'Video Gallery', '', '', '', '', '', '', '', '', 0, 0),
(3, 0, 'References', '', '', '', '', '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `haberler`
--

CREATE TABLE IF NOT EXISTS `haberler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t01` varchar(256) NOT NULL,
  `t02` varchar(256) NOT NULL,
  `t03` varchar(256) NOT NULL,
  `t04` varchar(256) NOT NULL,
  `t05` varchar(256) NOT NULL,
  `jum` int(11) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Tablo döküm verisi `haberler`
--

INSERT INTO `haberler` (`id`, `t01`, `t02`, `t03`, `t04`, `t05`, `jum`, `statu`) VALUES
(3, 'BEYMEN ARMADA AVM / ANKARA', '', 'ARMOURCOAT dekoratif boya çalışmaları yapılmıştır.', '', '14.jpg', 0, 0),
(5, 'TİTANİC OTEL BELEK / ANTALYA', '', 'ARMOURCOAT dekoratif boya çalışmaları yapılmıştır.', '', '14.jpg', 0, 0),
(6, 'TİTANİC OTEL LARA / ANTALYA', '', 'ARMOURCOAT dekoratif boya çalışmaları yapılmıştır.', '', '14.jpg', 0, 0),
(7, 'BİSSE MAĞAZALARI', '', 'ARMOURCOAT dekoratif boya çalışmaları yapılmıştır.', '', '14.jpg', 0, 0),
(9, 'BODRUM HAVA LİMANI', '', 'ARMOURCOAT.SMOOTH dekoratif boya çalışmaları yapılmıştır.', '', '14.jpg', 0, 0),
(15, 'PROMAX OFİS/KADIKÖY/İSTANBUL', '', 'ARMOURCOAT dekoratif boya çalışmaları yapılmıştır.', '', '14.jpg', 0, 0),
(18, 'FOUR SEASON HOTEL - BAKÜ', '', 'Four Season otelinin SPA alanında ARMOURCOAT PERLATA dekoratif boya çalışmaları yapılmıştır.', '', '14.jpg', 0, 0),
(19, 'BEYMEN - ERBIL', '', 'Beymen''in Erbil''deki mağazasında ARMOURCOAT PERLATA dekoratif boya yapıldı. Bu uygulama mağazanın genelinde uygulanmıştır.', '', '14.jpg', 0, 0),
(20, 'SOCHI OLIMPIYAT OTEL', '', 'Sochi Olimpiyat Otelinin genel alanlarında ARMOURCAST CORAL kullanılmıştır.', '', '14.jpg', 0, 0),
(21, 'GOOGLE', '', 'Google''ın İstanbul''da ki merkez binasında ARMOURCOAT PERLATA uygulaması yapılmıştır.', '', '14.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kategoriler`
--

CREATE TABLE IF NOT EXISTS `kategoriler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `t01` varchar(256) NOT NULL,
  `t02` varchar(256) NOT NULL,
  `t03` text NOT NULL,
  `t04` varchar(256) NOT NULL,
  `t05` varchar(256) NOT NULL,
  `seo` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `keys` varchar(256) NOT NULL,
  `desc` varchar(256) NOT NULL,
  `jum` int(11) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Tablo döküm verisi `kategoriler`
--

INSERT INTO `kategoriler` (`id`, `aid`, `t01`, `t02`, `t03`, `t04`, `t05`, `seo`, `title`, `keys`, `desc`, `jum`, `statu`) VALUES
(1, 0, 'Armourcoat Polished Plaster', '', 'Armourcoat bazı müşterilerin ve projelerin zaman zaman orijinal ve benzersiz kaplama talepleri olmasını takdir eder. İster basit renk uyumu olsun, ister karmaşık geometrik dizayn olsun yada ister farklı dokuların karmaşık birleşimi olsun, Armourcoat''un çok yönlü malzemeleri, yaratıcı vizyonu ve sıva sanatına yaklaşımı ile olağanüstü sonuçları garanti eder. \nDoğanın kendisine benzemek için durmaksızın doğal malzemelerle, şaşırtıcı sonuçlarla sürekli denemeler yaparız. Yaratıcılığımız cilalı sıvama alanında bize sanatsal bir üstünlük verir.', '', '', 'armourcoat-polished-plaster', 'a11', 'a1', 'a11', 0, 0),
(2, 0, 'Armour Color', '', 'Çarpıcı görünümü ve inanılmaz dayanıklılığı birleştiren yeni ArmourColor dekoratif duvar kaplama çeşitleri muhteşemlikten daha az bir şey sunmuyor. Sadece harikulade görünüm ve his için dizayn edilmemiş aynı zamanda uzun ömürlü ve hijyeniklerdir.', '', '', 'armour-color', 'a2', 'a2', 'a2', 0, 0),
(3, 0, 'Armour Cast', '', 'ArmourCast taş görüntüsü ve hissi veren müthiş 3 boyutlu alçı dizaynları yaratmak için kullanılır – iç ve dış uygulamalar için elverişlidir. Materyal ve yapım aşaması, Armourcoat Polished Plaster kaplamanın elle uygulanırlığı için fazla komplex olan şekiller ve dizaynlar yaratmak için idealdir. Armourcoat Surface Finishes, ArmourCast dizayn ve ArmourCast kaplama seçeneklerinden panel üretimine ve montajına kadar komple servis sağlar. Lütfen Kuzey Amerika''da ulaşılabilir olmadığını not ediniz.', '', '', 'armour-cast', '', '', '', 0, 0),
(4, 0, 'Sculptural', '', 'Armourcoat Sculptural görünmez yapısal duvar yüzeyi tasarımı dizisidir. Sculptural® duvarları alt tabakaya bağlanmış olan önceden dökme panel serilerinden oluşturulmuşturpanel birleşim yerleri daha sonra doldurulmuş ve kumlanmış ve son dekorasyon yüzeye uygulanmıştır. Sculptural® tasarımları, hatasızlığı ve elle hazırlanmış olmanın ruhunun korunmasını bir arada tutabilmek için, bilgisayar destekli tasarımları geleneksel heykeltraşlıkla birleştirmiştir.', '', '', 'sculptural', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kilit`
--

CREATE TABLE IF NOT EXISTS `kilit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bir` varchar(128) NOT NULL,
  `iki` varchar(128) NOT NULL,
  `statu` int(11) NOT NULL,
  `ops` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Tablo döküm verisi `kilit`
--

INSERT INTO `kilit` (`id`, `bir`, `iki`, `statu`, `ops`) VALUES
(1, 'info@linkmice.com', '123456', 1, 'LinkMice');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `palet`
--

CREATE TABLE IF NOT EXISTS `palet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `t01` varchar(256) NOT NULL,
  `t02` varchar(256) NOT NULL,
  `t03` varchar(256) NOT NULL,
  `jum` int(11) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `pdf`
--

CREATE TABLE IF NOT EXISTS `pdf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` varchar(32) NOT NULL,
  `bid` varchar(32) NOT NULL,
  `t01` varchar(128) NOT NULL,
  `t02` varchar(128) NOT NULL,
  `t03` varchar(128) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=102 ;

--
-- Tablo döküm verisi `pdf`
--

INSERT INTO `pdf` (`id`, `aid`, `bid`, `t01`, `t02`, `t03`, `statu`) VALUES
(8, 'urun', '11', 'travertine.pdf\r\n', '', '', 0),
(9, 'urun', '12', 'armuralia.pdf', '', '', 0),
(10, 'urun', '10', 'spatulata.pdf', '', '', 0),
(11, 'urun', '9', 'dragged.pdf', '', '', 0),
(12, 'urun', '8', 'colourwash.pdf', '', '', 0),
(13, 'urun', '7', 'pitted.pdf', '', '', 0),
(14, 'urun', '6', 'smooth.pdf', '', '', 0),
(15, 'urun', '16', 'perlata.pdf', '', '', 0),
(16, 'urun', '17', 'tactite.pdf', '', '', 0),
(17, 'teknik', '1', '', 'AS1 Internal Areas Exposed To Moisture', 'AS1InternalAreasExposedToMoisture.pdf', 0),
(18, 'teknik', '1', 'Building Specification Sheet', 'BSS1 - Armourcoat Smooth Finish', 'bss1smoothfinish.pdf', 0),
(19, 'teknik', '1', 'Building Specification Sheet', 'BSS2 - Armourcoat Cloudy Finish', 'bss2cloudyfinish.pdf', 0),
(20, 'teknik', '1', 'Building Specification Sheet', 'BSS3 - Armourcoat Pitted Finish', 'bss3pittedfinish.pdf', 0),
(21, 'teknik', '1', 'Building Specification Sheet', 'BSS4 - Armourcoat Dragged Finish', 'bss4draggedfinish.pdf', 0),
(22, 'teknik', '1', 'Building Specification Sheet', 'BSS5 - Armourcoat Travertine Finish', 'bss5travertinefinish.pdf', 0),
(23, 'teknik', '1', 'Building Specification Sheet', 'BSS6 - Armourcoat Spatulata Finish', 'bss6spatulatafinish.pdf', 0),
(24, 'teknik', '1', 'Building Specification Sheet', 'BSS7 - Armourcoat Armuralia Finish', 'bss7armuraliafinish.pdf', 0),
(25, 'teknik', '1', 'Building Specification Sheet', 'BSS8 - Armourcoat Banded Effect', 'bss8bandedeffect.pdf', 0),
(26, 'teknik', '1', 'Building Specification Sheet', 'BSS9 - External Application Armourcoat Of Sealed Armourcoat Polished Plaster', 'bss9externalusepittedfinish.pdf', 0),
(27, 'teknik', '1', 'Building Specification Sheet', 'BSS10 - Raised Logo Effect', 'bss10raisedlogoeffect.pdf', 0),
(28, 'teknik', '1', 'Building Specification Sheet', 'BSS11 - Armourcoat "Recessed Logo" Effect', 'bss11recessedlogo.pdf', 0),
(29, 'teknik', '1', 'Building Specification Sheet', 'BSS12 - Armourcoat Stone Block Effect', 'bss12stoneblockeffect.pdf', 0),
(30, 'teknik', '1', 'Building Specification Sheet', 'BSS13 - Armourcoat \\"Faux Panel\\" Effect', 'bss13fauxpaneleffect.pdf', 0),
(31, 'teknik', '1', 'Building Specification Sheet', 'BSS17 - Armourcoat Ashlar Blockwork Effect', 'bss17ashlarblockeffect.pdf', 0),
(32, 'teknik', '1', 'Building Specification Sheet', 'BSS18 - Armourcoat SMG Finish', 'bss18smgfinish.pdf', 0),
(33, 'teknik', '1', 'Building Specification Sheet', 'BSS19 - Armourcoat Pitted Finish with Paste Fill', 'bss19pittedfinishpastefill.pdf', 0),
(34, 'teknik', '1', 'Care & Maintenance Sheet', 'CMS1 - Care & Maintenance Sheet 1 for Polished Plaster', 'cms1pp.pdf', 0),
(35, 'teknik', '1', 'Care & Maintenance Sheet', 'CMS3 - Care & Maintenance Sheet 3 for Armourcoat Decorative Coatings', 'cms3decorativecoatings.pdf', 0),
(36, 'teknik', '1', 'Drawing Detail Sheet', 'DS1 - Detailing for skimmed or taped jointed plasterboard substrates', 'ds1detailingforskimmedortapedjointedplasterboardsubstrates.pdf', 0),
(37, 'teknik', '1', 'Drawing Detail Sheet', 'DS2 - Detailing for Armourcoat Anticrack on plasterboard substrates', 'ds2detailingforarmourcoatanticrackonplasterboardsubstrates.pdf', 0),
(38, 'teknik', '1', 'Drawing Detail Sheet', 'DS3 - Detailing for Armourcoat finishes on brick, block, or concrete substrates', 'ds3detailingforarmourcoatfinishesonbrickblockorconcretesubstrates.pdf', 0),
(39, 'teknik', '1', 'Drawing Detail Sheet', 'DS4 - Detailing for Armourcoat finishes on Glasroc or GRG substrates', 'ds4detailingforarmourcoatfinishesonglasrocorgrgsubstrates.pdf', 0),
(40, 'teknik', '1', 'Drawing Detail Sheet', 'DS7 - Detailing for Armourcoat finishes on MDF substrates', 'ds7detailingforarmourcoatfinishesonmdfsubstrates.pdf', 0),
(41, 'teknik', '1', 'Drawing Detail Sheet', 'DS9 - Detailing for Armourcoat finishes on Bluclad substrates', 'ds9detailingforarmourcoatfinishesonblucladsubstrates.pdf', 0),
(42, 'teknik', '1', 'General Requirements', 'General Requirements', 'generalrequirementswithanticrack.pdf', 0),
(43, 'teknik', '1', 'General Requirements', 'General Requirements with AntiCrack', 'generalrequirementswithanticrack.pdf', 0),
(47, 'teknik', '1', 'Health and safety Policy Statement', 'Health and safety Policy Statement', 'healthandsafetypolicystatement.pdf', 0),
(48, 'teknik', '1', 'Product Data Sheet', 'PDS1 - Polished Plaster P01, P20, P30, P66', 'pds1polishedplaster.pdf', 0),
(49, 'teknik', '1', 'Product Data Sheet', 'PDS2 - Spatulata', 'pds2spatulata.pdf', 0),
(50, 'teknik', '1', 'Product Data Sheet', 'PDS3 - Armuralia', 'pds3armuralia.pdf', 0),
(51, 'teknik', '1', 'Product Data Sheet', 'PDS5 - Keycoat', 'pds5keycoat.pdf', 0),
(52, 'teknik', '1', 'Product Data Sheet', 'PDS8 - K40 Primer', 'pds8k40primer.pdf', 0),
(53, 'teknik', '1', 'Product Data Sheet', 'PDS9 - Aquawax', 'pds9aquawax-voc.pdf', 0),
(54, 'teknik', '1', 'Product Data Sheet', 'PDS10 - Armoursil Impregnator', 'pds10armoursilimpregnator.pdf', 0),
(55, 'teknik', '1', 'Product Data Sheet', 'PDS12 - White Wax', 'pds12whitewax.pdf', 0),
(56, 'teknik', '1', 'Product Data Sheet', 'PDS14 - R29 Resin', 'pds14r29resin.pdf', 0),
(57, 'teknik', '1', 'Product Data Sheet', 'PDS15 - Polished Plaster Colourwash', 'pds15polishedplastercolourwash.pdf', 0),
(58, 'teknik', '1', 'Product Data Sheet', 'PDS24 - Sealer 56', 'pds24sealer56.pdf', 0),
(59, 'teknik', '1', 'Product Data Sheet', 'PDS25 - Ecowax', 'pds25ecowax.pdf', 0),
(60, 'teknik', '1', 'Product Data Sheet', 'PDS26 - Eco Primer', 'pds26ecoprimer.pdf', 0),
(61, 'teknik', '1', 'Product Data Sheet', 'PDS29 - Resin Gauging Liquid 31', 'pds29resingaugingliquid31.pdf', 0),
(62, 'teknik', '1', 'Product Data Sheet', 'PDS13 - R13 Resin', 'pds13r13resinnew.pdf', 0),
(63, 'teknik', '1', 'Substrate Specification Sheet', 'SSS1 - Plasterboard Drylining', 'sss1plasterboarddrylininginstallationandfinishing.pdf', 0),
(64, 'teknik', '1', 'Substrate Specification Sheet', 'SSS3 - Brick, Block, Concrete', 'sss3brickblockconcrete.pdf', 0),
(65, 'teknik', '1', 'Substrate Specification Sheet', 'SSS4 - GRG', 'sss4grg.pdf', 0),
(66, 'teknik', '1', 'Substrate Specification Sheet', 'SSS5 - Previously Decorated', 'sss5previouslydecorated.pdf', 0),
(67, 'teknik', '1', 'Substrate Specification Sheet', 'SSS6 - Sand Cement', 'sss6sandcement.pdf', 0),
(68, 'teknik', '1', 'Substrate Specification Sheet', 'SSS7 - MDF', 'sss7mdf.pdf', 0),
(69, 'teknik', '1', 'Substrate Specification Sheet', 'SSS9 - Bluclad', 'sss9bluclad.pdf', 0),
(70, 'teknik', '1', 'Substrate Specification Sheet', 'SSS10 - Exterior Application', 'sss10exteriorapplication.pdf', 0),
(71, 'teknik', '1', 'Substrate Specification Sheet', 'SSS2 - Substrate Specification - AntiCrack', 'sss2plasterboarddrylining-anticrack.pdf', 0),
(72, 'teknik', '1', 'Terms & Conditions', 'Terms and Conditions of Use for Document', 'termsandconditionsofusefordocuments.pdf', 0),
(73, 'teknik', '1', 'Warranty Information', 'Ten Year Limited Materials Warranty For Interior Use', '10yearinteriorwarrantypp.pdf', 0),
(74, 'teknik', '1', 'Warranty Information', 'Five Year Limited Materials Warranty For Exterior Use', '5yearexteriorwarrantypp.pdf', 0),
(75, 'teknik', '2', 'Building Specification Sheet', 'BSS301 - ArmourColor Perlata Finish', 'bss301-armourcolorperlatafinish.pdf', 0),
(76, 'teknik', '2', 'Building Specification Sheet', 'BSS303 - ArmourColor Tactite Finish', 'bss303-armourcolortactitefinish.pdf', 0),
(77, 'teknik', '2', 'Building Specification Sheet', 'BSS302 - ArmourColor Escenta Finish', 'bss302-armourcolorescentafinish.pdf', 0),
(78, 'teknik', '2', 'Environmental Statement', 'ES5 - ArmourColor', 'es5environmentalstatementforarmourcolor.pdf', 0),
(79, 'teknik', '2', 'Health and safety Policy Statement', 'Health and safety Policy Statement', 'healthandsafetypolicystatement.pdf', 0),
(80, 'teknik', '2', 'Product Data Sheet', 'PDS10 - Armoursil Impregnator', 'pds10armoursilimpregnator.pdf', 0),
(81, 'teknik', '2', 'Product Data Sheet', 'PDS24 - Sealer 56', 'pds24sealer56.pdf', 0),
(82, 'teknik', '2', 'Product Data Sheet', 'PDS26 - Eco Primer', 'pds26ecoprimer.pdf', 0),
(83, 'teknik', '2', 'Substrate Specification Sheet', 'SSS301 - ArmourColor', 'sss301armourcolor.pdf', 0),
(84, 'teknik', '2', 'Terms & Conditions', 'Terms and Conditions of Use for Document', 'termsandconditionsofusefordocuments.pdf', 0),
(85, 'teknik', '2', 'Warranty Information', 'WS301 - Decorative Wall Coatings 1 year Limited Materials Warranty For Interior Use', '1yearwarrantyforarmourcolor.pdf', 0),
(86, 'teknik', '3', 'Environmental Statement', 'ES3 - Armourcast', 'es3environmentalstatementforarmourcast.pdf', 0),
(87, 'teknik', '3', 'Health and safety Policy Statement', 'Health and safety Policy Statement', 'healthandsafetypolicystatement.pdf', 0),
(88, 'teknik', '3', 'Product Data Sheet', 'PDS6 - ArmourCast', 'pds6armourcast.pdf', 0),
(89, 'teknik', '3', 'Product Data Sheet', 'PDS10 - Armoursil Impregnator', 'pds10armoursilimpregnator.pdf', 0),
(90, 'teknik', '3', 'Product Specification Document', 'Armourcast Product Specification', 'armourcastproductspecification.pdf', 0),
(91, 'teknik', '3', 'Substrate Specification Sheet', 'SSS12 - Wall Structure to Support ArmourCast Panels', 'sss12wallstructuretosupportarmourcastpanels.pdf', 0),
(92, 'teknik', '3', 'Terms & Conditions', 'Terms and Conditions of Use for Document', 'termsandconditionsofusefordocuments.pdf', 0),
(93, 'teknik', '4', 'Care & Maintenance Sheet', 'CMS4 - Care & Maintenance Sheet for Armourcoat SCULPTURAL', 'cms4armourcoatsculptural.pdf', 0),
(94, 'teknik', '4', 'Environmental Statement', 'ES4 - SCULPTURAL', 'es4environmentalstatementforsculptural.pdf', 0),
(95, 'teknik', '4', 'Health and safety Policy Statement', 'Health and safety Policy Statement', 'healthandsafetypolicystatement.pdf', 0),
(96, 'teknik', '4', 'Product Data Sheet', 'PDS23 - Sculptural Bondplast', 'pds23sculpturalbondplastupdate.pdf', 0),
(97, 'teknik', '4', 'Substrate Specification Sheet', 'SSS11 - Sculptural', 'sss11sculptural.pdf', 0),
(98, 'teknik', '4', 'Terms & Conditions', 'Terms and Conditions of Use for Document', 'termsandconditionsofusefordocuments.pdf', 0),
(99, 'teknik', '4', 'Warranty Information', '10 Year Limited Panel Warranty for Interior Use', 'sculptural10yearpanelwarranty.pdf', 0),
(100, 'urun', '25', 'KONCRETE_2PP.pdf', '', '', 0),
(101, 'urun', '26', 'SMG_2PP.pdf', '', '', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` varchar(256) NOT NULL,
  `bid` int(11) NOT NULL,
  `t01` varchar(256) NOT NULL,
  `t02` varchar(256) NOT NULL,
  `t03` varchar(256) NOT NULL,
  `t04` varchar(256) NOT NULL,
  `jum` int(11) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=878 ;

--
-- Tablo döküm verisi `photos`
--

INSERT INTO `photos` (`id`, `aid`, `bid`, `t01`, `t02`, `t03`, `t04`, `jum`, `statu`) VALUES
(84, 'urunpalet', 6, 'SMX8047.jpg', 'SMX80471.jpg', 'SMX8047', '0', 0, 0),
(85, 'kategori', 3, '72.jpg', '73.jpg', '0', '0', 0, 0),
(86, 'kategori', 3, '82.jpg', '83.jpg', '0', '0', 0, 0),
(87, 'urunpalet', 6, 'SMX8054.jpg', 'SMX80541.jpg', 'SMX8054', '0', 0, 0),
(88, 'kategori', 3, '92.jpg', '93.jpg', '0', '0', 0, 0),
(89, 'kategori', 3, '102.jpg', '103.jpg', '0', '0', 0, 0),
(90, 'urunpalet', 6, 'SMX8112.jpg', 'SMX81121.jpg', 'SMX8112', '0', 0, 0),
(91, 'kategori', 3, '113.jpg', '114.jpg', '0', '0', 0, 0),
(92, 'kategori', 3, '123.jpg', '124.jpg', '0', '0', 0, 0),
(93, 'kategori', 3, '133.jpg', '134.jpg', '0', '0', 0, 0),
(94, 'kategori', 3, '143.jpg', '144.jpg', '0', '0', 0, 0),
(95, 'kategori', 3, '152.jpg', '153.jpg', '0', '0', 0, 0),
(96, 'kategori', 3, '162.jpg', '163.jpg', '0', '0', 0, 0),
(97, 'kategori', 3, '172.jpg', '173.jpg', '0', '0', 0, 0),
(98, 'kategori', 3, '182.jpg', '183.jpg', '0', '0', 0, 0),
(99, 'kategori', 3, '20.jpg', '201.jpg', '0', '0', 0, 0),
(100, 'kategori', 3, '211.jpg', '212.jpg', '0', '0', 0, 0),
(101, 'kategori', 3, '221.jpg', '222.jpg', '0', '0', 0, 0),
(102, 'kategori', 3, '231.jpg', '232.jpg', '0', '0', 0, 0),
(103, 'kategori', 3, '241.jpg', '242.jpg', '0', '0', 0, 0),
(104, 'kategori', 3, '251.jpg', '252.jpg', '0', '0', 0, 0),
(105, 'kategori', 3, '26.jpg', '261.jpg', '0', '0', 0, 0),
(106, 'kategori', 3, '311.jpg', '312.jpg', '0', '0', 0, 0),
(107, 'kategori', 3, '321.jpg', '322.jpg', '0', '0', 0, 0),
(108, 'kategori', 3, '331.jpg', '332.jpg', '0', '0', 0, 0),
(109, 'kategori', 3, '34.jpg', '341.jpg', '0', '0', 0, 0),
(110, 'kategori', 3, '35.jpg', '351.jpg', '0', '0', 0, 0),
(111, 'kategori', 3, '36.jpg', '361.jpg', '0', '0', 0, 0),
(112, 'kategori', 3, '37.jpg', '371.jpg', '0', '0', 0, 0),
(113, 'kategori', 3, '38.jpg', '381.jpg', '0', '0', 0, 0),
(862, 'genelgaleri', 3, '1.jpg', '11.jpg', '0', '0', 0, 0),
(863, 'genelgaleri', 3, '2.jpg', '21.jpg', '0', '0', 0, 0),
(864, 'genelgaleri', 3, '3.jpg', '31.jpg', '0', '0', 0, 0),
(865, 'genelgaleri', 3, '4.jpg', '41.jpg', '0', '0', 0, 0),
(869, 'genelgaleri', 1, '119.jpg', '120.jpg', 'Lorem Ipsum', '0', 0, 0),
(870, 'genelgaleri', 1, '121.jpg', '122.jpg', 'Lorem Ipsum', '0', 0, 0),
(871, 'genelgaleri', 1, '123.jpg', '124.jpg', 'Lorem Ipsum', '0', 0, 0),
(875, 'genelgaleri', 2, '128.jpg', '0', 'http://www.youtube.com/watch?v=di-JgQehtpQ', '0', 0, 0),
(876, 'genelgaleri', 2, '129.jpg', '130.jpg', 'http://www.youtube.com/watch?v=di-JgQehtpQ', '0', 0, 0),
(877, 'genelgaleri', 2, '131.jpg', '132.jpg', 'http://www.youtube.com/watch?v=di-JgQehtpQ', '0', 0, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `referanslar`
--

CREATE TABLE IF NOT EXISTS `referanslar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t01` varchar(256) NOT NULL,
  `t02` text NOT NULL,
  `t03` varchar(256) NOT NULL,
  `t04` varchar(256) NOT NULL,
  `t05` varchar(256) NOT NULL,
  `jum` int(11) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Tablo döküm verisi `referanslar`
--

INSERT INTO `referanslar` (`id`, `t01`, `t02`, `t03`, `t04`, `t05`, `jum`, `statu`) VALUES
(1, 'Porche Mağazası - Nişantaşı', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam', '111.jpg', '112.jpg', 0, 0),
(4, 'Koç Holding Binası - Nakkaştepe', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam', '114.jpg', '112.jpg', 0, 0),
(5, 'Bisse - Konya', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam', '113.jpg', '112.jpg', 0, 0),
(6, 'Bisse - Topçular', '', '', '115.jpg', '112.jpg', 0, 0),
(7, 'Astra Zenika  – Maslak', '', '', '118.jpg', '112.jpg', 0, 0),
(8, 'B.B.D.O – Maslak', '', '', '117.jpg', '112.jpg', 0, 0),
(9, 'Capital City - Rusya', '', '', '116.jpg', '112.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sayfa`
--

CREATE TABLE IF NOT EXISTS `sayfa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` varchar(128) NOT NULL,
  `t01` varchar(128) NOT NULL,
  `t02` text NOT NULL,
  `t03` varchar(256) NOT NULL,
  `t04` varchar(256) NOT NULL,
  `t05` varchar(256) NOT NULL,
  `t06` varchar(256) NOT NULL,
  `bg` varchar(256) NOT NULL,
  `seo` varchar(128) NOT NULL,
  `title` varchar(256) NOT NULL,
  `desc` text NOT NULL,
  `keys` varchar(256) NOT NULL,
  `jum` int(11) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Tablo döküm verisi `sayfa`
--

INSERT INTO `sayfa` (`id`, `aid`, `t01`, `t02`, `t03`, `t04`, `t05`, `t06`, `bg`, `seo`, `title`, `desc`, `keys`, `jum`, `statu`) VALUES
(1, '', 'ÇEKİCİ, YENİLİKÇİ, YARATICI', '<p style="margin: 20px 0px 0px; padding: 0px; border: 0px; outline: 0px; font-size: 14px; vertical-align: baseline; display: table; line-height: 18px; color: #505050; font-family: calibriregular;">Estetis firması 2002 yılında dekorasyon alanında hizmet vermeye başlamış, 2010 yılında farklı sekt&ouml;re y&ouml;nelme sebebi ile kapatma kararı almışlardır. Bu tarih itibari ile Estetis b&uuml;nyesinde kurulumundan itibaren g&ouml;rev yapan ve proje m&uuml;d&uuml;r&uuml; olarak g&ouml;revlerini tamamlayan Ali Arslan &ndash; Mine Şimşek 10 yıllık tecrubesine dayanarak Armourcoat firmasının T&uuml;rkiye distrib&uuml;t&ouml;rl&uuml;ğ&uuml;n&uuml; almıştır. Bu deneyim ve tecr&uuml;be ile Bayrağı devir alan Estetis Arslan Group Ltd daha dinamik bir kadro ve değişmeyen uygulayıcı ekibi ile hizmet vermeye devam etmektedir.</p>\r\n<p style="margin: 20px 0px 0px; padding: 0px; border: 0px; outline: 0px; font-size: 14px; vertical-align: baseline; display: table; line-height: 18px; color: #505050; font-family: calibriregular;">İngiliz Armourcoat firmasının T&uuml;rkiye Distrib&uuml;t&ouml;r&uuml; olarak ARMOURCOAT ve ARMOURCAST &uuml;r&uuml;nlerinin tek uygulayıcısıdır. Orta doğu ve Birleşik Arap &uuml;lkelerinde de uygulama hizmeti vermektedir. Dekoratif sıva, boya ve duvar kaplamaları olarak &ccedil;ok geniş ve farklı bir &uuml;r&uuml;n yelpazesine sahiptir.</p>\r\n<p style="margin: 20px 0px 0px; padding: 0px; border: 0px; outline: 0px; font-size: 14px; vertical-align: baseline; display: table; line-height: 18px; color: #505050; font-family: calibriregular;">10 yıllık sekt&ouml;r tecr&uuml;besindeki referansları ile hizmet kalitesinden &ouml;d&uuml;n vermeden &ccedil;ok ayrıcalıklı bir yer edinmiş, bu alanda ilklerin &ouml;nc&uuml;s&uuml; olmuştur. Uzman kadrosu ile ( b&uuml;nyesinde en az 5 yıllık hizmet yapmış ) uygulama garantisi vererek hizmet sunmaktadır.</p>\r\n<p style="margin: 20px 0px 0px; padding: 0px; border: 0px; outline: 0px; font-size: 14px; vertical-align: baseline; display: table; line-height: 18px; color: #505050; font-family: calibriregular;">ARMOURCAST Dekoratif taş d&ouml;k&uuml;m ( r&ouml;lyef paneller ) ve ARMOURCOAT Taş efektli duvar kaplamaları ile sayısız &ouml;nemli ve prestijli projede yer almıştır.</p>\r\n<p style="margin: 20px 0px 0px; padding: 0px; border: 0px; outline: 0px; font-size: 14px; vertical-align: baseline; display: table; line-height: 18px; color: #505050; font-family: calibriregular;">Dekoratif duvar kaplamalarımız Taş efektli olup, taşa en yakın g&ouml;rsel etkiyi vererek bir o kadar mukavemetini de sağlamaktadır. Her t&uuml;rl&uuml; Mermer, Br&uuml;t Beton, Doğal Taş, Traverten, Granit, Kum, Ahşap, S&uuml;et, Deri vs gibi &ouml;zel dekoratif duvar kaplamalarını mevcuttur. Naturel yapısını hi&ccedil; bozmadan ilk g&uuml;nk&uuml; etkisini yıllarca korumak en &uuml;st&uuml;n nitelikleri arasındadır.</p>', '', '', '', '', 'Nemec-showroom-2010-031.jpg', 'hakkimizda', 'Hakkımızda | Estetis', '', '', 0, 0),
(2, '', 'CAREER', '<p><span style="color: #353535; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-align: justify;">S&uuml;rekli yenilenen teknolojide, tasarım &ccedil;ağına dekoratif anlamda kreatif &ccedil;&ouml;z&uuml;mler sunmak, g&uuml;n&uuml;m&uuml;z&uuml;n mimari &ccedil;izgisine ayak uydurmak kısaca renk katmaktır. Bu anlayışla gelişen ve yenilenen teknolojiyi yakından takip ederek yenilenmek, hizmet anlayışımızı daha ileriye taşımak, geliştirmek ve alternatif &uuml;retmek en b&uuml;y&uuml;k hedefimizdir.</span></p>', '', '', '', '', 'z1b.jpg', 'amacimiz', 'Amacımız', '', 'amaç, estetis, misyon', 0, 0),
(3, '', 'BİZ KİMİZ', '', '', '', '', '', 'p101.jpg', 'bizkimiz', 'Biz Kimiz', '', 'biz estetis ekip', 0, 0),
(4, '', 'HABERLER', '', '', '', '', '', 'ARMOURCAST-TSUNAMI-21.jpg', 'haberler', 'Haberler', 'Estetis''den haberler...', 'estetis, haberler, referans', 0, 0),
(5, '', 'REFERANSLAR', '', '', '', '', '', 'wew.jpg', 'referanslar', 'Referanslar', 'Estetis referansları.', 'estetis, referans', 0, 0),
(6, '', 'KATEGORİ', '', '', '', '', '', '', '', '', '', '', 0, 0),
(7, '', 'DETAY', '', '', '', '', '', '', '', '', '', '', 0, 0),
(8, '', 'ANASAYFA', '', '', '', '', '', '', '', 'Link Mice', 'Link Mice', 'Link Mice', 0, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` varchar(32) NOT NULL,
  `bid` varchar(32) NOT NULL,
  `t01` varchar(128) NOT NULL,
  `t02` varchar(128) NOT NULL,
  `t03` varchar(128) NOT NULL,
  `t04` varchar(256) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=111 ;

--
-- Tablo döküm verisi `slider`
--

INSERT INTO `slider` (`id`, `aid`, `bid`, `t01`, `t02`, `t03`, `t04`, `statu`) VALUES
(8, 'sayfa', '5', 'dilek-yildirim2.jpg', '"a"', '"d"', '', 0),
(10, 'galeri', '2', 'ali-arslan2.jpg', 'aa', 'ss', '', 0),
(11, 'galeri', '2', 'mine-simsek4.jpg', 'aa', 'ss', '', 0),
(12, 'galeri', '2', 'dilek-yildirim4.jpg', 'aa', 'ss', '', 0),
(13, 'galeri', '2', 'ARMOURCAST-TUSUNAMI1.jpg', 'a', 'd', '', 0),
(14, 'galeri', '2', 'dilek-yildirim5.jpg', 'a', 's', '', 0),
(15, 'galeri', '2', 'mine-simsek5.jpg', 'a', 's', '', 0),
(20, 'sayfa', '2', 'img0033.jpg', 'test', 'test', '', 0),
(21, 'sayfa', '2', 'img0041.jpg', 'q', 'w', '', 0),
(24, 'kategori', '5', 'dilek-yildirim10.jpg', '', '', '', 0),
(34, 'urun', '1', 'mine-simsek8.jpg', '', '', '', 0),
(43, 'sayfa', '1', 'img0072.jpg', 'aaa', 'ddd', '2', 0),
(45, 'urun', '4', 'img0037.jpg', 'dsfsdf', 'sffsdf', '0', 0),
(47, 'sayfa', '4', 'img0033.jpg', '', '', '4', 0),
(48, 'sayfa', '3', 'img0044.jpg', '', '', '1', 0),
(55, 'kategori', '2', 'armourcolor11.jpg', '', 'Atelier Showroom by Estetis', '2', 0),
(56, 'kategori', '2', 'armourcolor2.jpg', '', 'Zoli by Estetis', '2', 0),
(60, 'urun', '16', 'perlata3.jpg', '', 'Rixos Premium Belek', '0', 0),
(61, 'urun', '16', 'perlata21.jpg', '', 'Rixos Premium Belek', '0', 0),
(62, 'urun', '17', 'tactite.jpg', '', 'BrewHouse Hotel', '0', 0),
(63, 'urun', '17', 'tactite2.jpg', '', 'by Estetis ', '0', 0),
(64, 'kategori', '4', 'sculptural2.jpg', '', 'AESİS by Estetis', '4', 0),
(65, 'kategori', '4', 'sculptural.jpg', '', 'AESİS by Estetis', '4', 0),
(67, 'kategori', '3', 'armourcast1.jpg', '', 'by Estetis ', '3', 0),
(70, 'kategori', '3', 'armourcast4.jpg', '', 'by Estetis ', '3', 0),
(71, 'urun', '6', 'smooth.jpg', '', 'by Estetis ', '0', 0),
(74, 'urun', '7', 'pitted1.jpg', '', 'by Estetis ', '0', 0),
(75, 'urun', '6', 'smooth21.jpg', '', 'Akmerkez Mudo by Estetis', '0', 0),
(76, 'urun', '7', 'pitted2.jpg', '', 'by Estetis ', '0', 0),
(77, 'urun', '8', 'colourwash.jpg', '', 'Louis Vuitton by Estetis', '0', 0),
(78, 'urun', '8', 'colourwash2.jpg', '', 'Louis Vuitton by Estetis', '0', 0),
(79, 'kategori', '1', 'colourwash1.jpg', '', 'Louis Vuitton by Estetis', '1', 0),
(80, 'kategori', '1', 'smooth22.jpg', '', 'Akmerkez Mudo by Estetis', '1', 0),
(81, 'urun', '9', 'dragged.jpg', '', 'Stan More House by Estetis', '0', 0),
(82, 'urun', '9', 'dragged2.jpg', '', 'by Estetis ', '0', 0),
(83, 'urun', '10', 'spatulata2.jpg', '', 'Jimmy Choo by Estetis', '0', 0),
(85, 'urun', '10', 'spatulata1.jpg', '', 'by Estetis ', '0', 0),
(86, 'urun', '11', 'travertine.jpg', '', 'by Estetis ', '0', 0),
(87, 'urun', '11', 'travertine2.jpg', '', 'by Estetis ', '0', 0),
(88, 'urun', '12', 'armuralia.jpg', '', 'Wagamama by Estetis', '0', 0),
(89, 'urun', '13', 'pitted3.jpg', '', 'by Estetis ', '0', 0),
(90, 'urun', '13', 'design.jpg', '', 'by Estetis ', '0', 0),
(91, 'urun', '13', 'design2.jpg', '', 'by Estetis ', '0', 0),
(92, 'urun', '14', 'design3.jpg', '', 'by Estetis ', '0', 0),
(93, 'urun', '14', 'design4.jpg', '', 'by Estetis ', '0', 0),
(94, 'urun', '15', 'anti.jpg', '', 'by Estetis ', '0', 0),
(95, 'urun', '19', 'modular2.jpg', '', 'by Estetis ', '0', 0),
(96, 'urun', '19', 'modular.jpg', '', 'by Estetis ', '0', 0),
(99, 'urun', '20', 'shop1.jpg', '', 'by Estetis ', '0', 0),
(100, 'urun', '20', 'shop.jpg', '', 'by Estetis ', '0', 0),
(101, 'urun', '21', 'colum.jpg', '', 'Ağaoğlu by Estetis', '0', 0),
(102, 'urun', '21', 'colum2.jpg', '', 'by Estetis ', '0', 0),
(103, 'urun', '22', 'mirror.jpg', '', 'by Estetis ', '0', 0),
(104, 'urun', '22', 'mirror2.jpg', '', 'by Estetis ', '0', 0),
(105, 'urun', '23', 'seat.jpg', '', 'by Estetis ', '0', 0),
(106, 'urun', '24', 'custom.jpg', '', 'Woodhams by Estetis', '0', 0),
(107, 'urun', '24', 'custom2.jpg', '', 'Faik Sönmez by Estetis', '0', 0),
(108, 'urun', '25', 'koncrete4.jpg', 'by KonCrete', '', '0', 0),
(110, 'urun', '26', 'smg1.jpg', '', 'by SMG', '0', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `teknik`
--

CREATE TABLE IF NOT EXISTS `teknik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `t01` varchar(256) NOT NULL,
  `t02` varchar(256) NOT NULL,
  `t03` text NOT NULL,
  `t04` varchar(256) NOT NULL,
  `t05` varchar(256) NOT NULL,
  `seo` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `keys` varchar(256) NOT NULL,
  `desc` varchar(256) NOT NULL,
  `jum` int(11) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Tablo döküm verisi `teknik`
--

INSERT INTO `teknik` (`id`, `aid`, `t01`, `t02`, `t03`, `t04`, `t05`, `seo`, `title`, `keys`, `desc`, `jum`, `statu`) VALUES
(1, 0, 'Polished Plaster', '', '', '', '', '', '', '', '', 0, 0),
(2, 0, 'Armour Color', '', '', '', '', '', '', '', '', 0, 0),
(3, 0, 'Armour Cast', '', '', '', '', '', '', '', '', 0, 0),
(4, 0, 'Sculptural', '', '', '', '', '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `urunler`
--

CREATE TABLE IF NOT EXISTS `urunler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `t01` varchar(256) NOT NULL,
  `t02` varchar(256) NOT NULL,
  `t03` text NOT NULL,
  `t04` text NOT NULL,
  `t05` varchar(256) NOT NULL,
  `seo` text NOT NULL,
  `title` text NOT NULL,
  `descr` text NOT NULL,
  `keys` text NOT NULL,
  `jum` int(11) NOT NULL,
  `statu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Tablo döküm verisi `urunler`
--

INSERT INTO `urunler` (`id`, `aid`, `t01`, `t02`, `t03`, `t04`, `t05`, `seo`, `title`, `descr`, `keys`, `jum`, `statu`) VALUES
(6, 1, 'Smooth', '', 'Pürüzsüz cila kaplama, yoğunluklu olarak Kuzey Italya''da Rönesns döneminde duvar kaplaması ve bezemeli fresklerin arka zemini için kullanılan ''marmorino stucco'' benzeridir. Yaygın olarak Venedik cilası olarak bilinen şey bu materyal ve tekniğin birleşimidir. Bu kaplamanın son görüntüsü onu uygulayan kişiye oldukça bağlıdır; düz, hafif alçılanmış bir yüzeyden çokça çapaklı ve mermerleştirilmiş etkisini elde etmeye kadar pek çok şeyi sağlayabilir.', '{"n1":"1","n3":"1","n5":"1","n7":"1","n9":"1"}', '', 'smooth', '', '', '', 0, 0),
(7, 1, 'Pitted', '', 'Bilenmiş yada hafif cilalanmış kireç taşı hissi ve görünümünü elde etmek için sağlam, daha damarlı görünüm daha az yüksek derecede cilalanmış yüzeyle kombine edilir. Uygulama monolitik taş yüzeyi yada doğal taş bloğu görünümü elde edilmeye çalışıldığında daha etkili olacaktır. Bu kaplama incelikli bir kaliteye sahiptir, her ne kadar geniş yüzeylerde dramatik bir görünüş sağlamasa da sanat ve moda çalışmalarında zengin arka fon olarak yoğunlukla kullanılır.', '', '', 'pitted', '', '', '', 0, 0),
(8, 1, 'Colourwash', '', 'Çukurlaştırılmış (Pitted) ve sürüklenmiş (Dragged) kaplama Colourwash uygulaması ile daha fazla geliştirilebilir. Saf renk maddesi, metalik ya da sedefli pudra ile kombine edilmiş şeffaf sır alçıya uygulanır, zemini zenginleştirir, renklerin zıtlıklarını ve tonların incelikli çeşitliliklerini açığa vurur. Colourwash kaplama zengin tonları ile geniş duvarlar, özel alanlar ve dramatik sahne arkası perdeleri için özellikle yaratıcı ve dramatik ışıklandırma ile beraber kullanıldığında idealdir.\n\n', '', '', 'colourwash', '', '', '', 0, 0),
(9, 1, 'Dragged', '', 'Aşınmış kayaların desenlerinden ilham alınmıştır. Güçlü yön kalitesi vurgusu yapabilmek için engebeli, kumlu, yarı cilalanmış yüzey zeminden akan çizik çizik olmuş dokuyla karakterize edilmiştir. Işıkta, doğal renkler üzerinde Dragged kaplama doğal taş görünümüne sahiptir, eski kayaların birbirlerine sürtünmeleri ile oluşan kesme işaretlerini yansıtan bir görünümü vardır. Derin tonlarda ve daha sonra yıkanmış renk kodlarında kullanılması uygulamanın yönlü etkisini büyük ölçüde geliştirir.', '', '', 'dragged', '', '', '', 0, 0),
(10, 1, 'Spatulata', '', 'Spatula kaplama tam bir lükstür. Mısırlılar tarafından öncülüğü yapılmış, Romalılar tarafından geliştirilmiştir. Venedikliler tarafından ''stucco lustro veneziano'' olarak bilinmeye başlanmıştır. 3-4 kat uygulama ile başarı yakalamayı ve küçük bir çelik bıçak yada mala ile camsı parlaklık sağlamayı mümkün kılan çalşıma özellikleri modern katkı malzemeleri yardımı ile müthiş geliştirilmiştir.', '', '', 'spatulata', '', '', '', 0, 0),
(11, 1, 'Travertine', '', 'Armuralia''nın 67 renk çeşidi mevcuttur. Zarif ipeksi pürüzsüz kaplama ve hassas renk varyasyonları ile en son projenize şıklık ve tarz getirebilir. Kireç kaymağı ve ezilmiş Bianco Carrara mermerinden yapilmiş olan Armuralia tamamen doğal bir üründür ve neredeyse hiç VOC içermez.', '', '', 'travertine', '', '', '', 0, 0),
(12, 1, 'Armuralia', '', 'Arttırılmış doku ve renk seçenekleriyle Armourcoat polished plaster''lar görkemli ve dayanıklı bir yüzeye sahip olmanızı sağlar.', '', '', 'armuralia', '', '', '', 0, 0),
(13, 1, 'Custom Finishes', '', 'Büyük çeşitlilikteki taş kaplama uygulamasında neredeyse sınırsız şekil ve dizayn çeşitliliği yaratma yeteneği ile, ArmourCast mimarlara ve tasarımcılara müthiş sanatsal faaliyet alanı sağlar.', '', '', 'custom-finishes', '', '', '', 0, 0),
(14, 1, 'Design Features', '', 'Armourcoat mimarlara ve tasarımcılara bantlama, nem yalıtımı, paneller, şirket logoları ve sutünları gibi büyüleyici ve yenilikçi tasarım özellikleri oluşturmak için geniş deneyim imkânları sunuyor. \n\nBanding aynı ya da farklı kaplamaları ya da dikkatle kontrol edilerek uygulanmış colourwash tekniğini kullanarak, bir kaplamayı diğerinin üzerine yerleştirmek suretiyle oluşturulur. Blockwork, doğal taşın ağırlığı ve kalınlığı olmaksızın üst düzeyde yapay taş etkisi yaratır. Taş duvarlarda bulunan doğal farklılıkarın yansıması olarak doku ve ton bloktan bloğa değişebilir. Panel tekniği, şeritleri ve bir kaplama birleşimini kullanır, zarif bir etki sağlar, büyük ve küçük alanlar için uygundur ve odaya bir uzunluk hissi katar. \n\nKurumsal logoların ve grafik çalışmalarının uygulamaları yüksek standartta alçı kaplaması ve detaylar gerektirir. Logo içerimi ve yüzey tasarımı uygulaması için özel teknikler geliştirmiş olarak Armourcoat bu alanda lider olarak anılmaktadır. Circular Colums and Curves bir miktar maliyetle cilalı taş görnümü verir. Bütük sutunlarda bile tek bir geniş taş parçasından keşilmişçesine tamamen farkedilmez görünebilir. Armourcoat en yüksek standartları mümkün kılmak için özel araçlar ve teknikler geliştirerek dünya çapında yüzlerce sütun alçılamıştır.', '', '', 'design-features', '', '', '', 0, 0),
(15, 1, 'Anti Crack', '', 'Armourcoat AntiCrack alçıpan inşaatının çatlama ve dayanıklılık direncini önemli ölçüde artıran elyaf ve reçine ile güçlendirilmiş alçıtaşı perdah sıvasıdır. Armourcoat AntiCrack, birbirinden ayrı levhalar arasındaki herhangi bir hareketi ya da çatlamayı önemli şekilde düşürdüğü kanıtlanmış olan üstün bir mukavemet gösterir. Armourcoat AntiCrack''ın yüksek basınç direnci tüm Armourcoat kaplamaları için sert ve dayananıklı arka plan sağlar.', '', '', 'anti-crack', '', '', '', 0, 0),
(16, 2, 'Perlata', '', 'Zarif ve lüks Perlata, görsel doku ve yönsel etki yaratan hafif kvılcım ve parıltılı özelliği ile zarif bir kaplamadır. Düşük parlaklıkla, ilgi çekici yüzey hareket ve ton ilgileri ile doludur. Yakın mesafelerde, eşşiz ve görsel olarak çarpıcı ancak uzaktan bakıldığında hafif görünen duvar kaplamasını yaratmak uygulayan kişinin eline bağlıdır.', '', '', 'perlata', '', '', '', 0, 0),
(17, 2, 'Tactite', '', 'Zahmetli iç mekanlar için yüksek performanslı çözüm olan Tactite sert, dayanıklı ve hijyenik duvar kılıfı yaratır. Bu kaplama son derece sağlam ve temizlemesi kolaydır, bu yüzden hastane, okul, ofis, yüksek trafiğin olduğu kamu alanları gibi temizliğin önemli olduğu ticari iç mekânlar için bir hayli uygundur. Aynı zamanda ev kullanımı içinde uygundur, Tactite ''yumuşak dokunuş'' hissi yaratarak ve suet ya da dokuma kaplaması ile sıcak, davetkâr bir iç mekân sunar.', '', '', 'tactite', '', '', '', 0, 0),
(19, 3, 'Modular Wall Systems', '', 'ArmourCast karmaşık geometrik desenlerden daha organik ve heykelsi yapılara kadar değişen geniş bir moduler panel designı yapmak için kullanılabilir. Paneller, desen ve dizaynın bir panelden diğerine doğal bir şekilde akacağı şekilde dizayn edilebilir. Dizayn içinde birleşim çizgilerini kaynaştırmak ta mümkündür, bu sayede yüzey bir dizi bitiştirilmiş panel şeklinde görülmez. Bu modulerliğin en önemli avantajlarından biri büyük duvar alanlarını tamamlamak için daha az kalıp gerektirmesidir.', '', '', 'modular-wall-systems', '', '', '', 0, 0),
(20, 3, 'Shop Fitting', '', 'Panel ve ekran öğelerine birleştirilmiş 3 boyutlu logo yada marka kimliği oluşturma yeteneği ile Armourcast mağaza içi tasarımı için muazzam bir faaliyet alanı sunar. ArmourCast bütün ekran öğerlerini, yere monte görüntü birimlerini, raf sergileri ve oyuk kutular yaratmak için kullanılabilir.', '', '', 'shop-fitting', '', '', '', 0, 0),
(21, 3, 'Columns', '', 'Gerçek taş sütunları çok pahalı ve ağır olmalarının yanı sıra sürekli beton ya da çelik kirişlerin döküldüğü modern bina uygulamaları için çok uygun değillerdir. ArmourCast taş görünüşlü ve hisli sütun ya da gömme ayak yapmak için idealdir. Tüm sütunlar genel olarak, biraraya getirlimiş ve üzeri yapısal desteklerle kaplanmış iki yarımdan oluşur. Yatay çizgiler, tabanda ve sütunbaşlarındaki yiv şeklinde süs ve dekoratif detaylar dizaynın içine katılabilir. Yuvarlak, oval, eliptik, kare, altıgen ya da asimetrik sütunların hepsi mümkündür.', '', '', 'columns', '', '', '', 0, 0),
(22, 3, 'Mirrors', '', 'Normalde tek bir taş parçasından ayna çerçevesi yapabilmek için geniş büyük bir yassı taş ve önemli miktarda oyma işçiliği gereklidir. ArmourCast ile tümüyle görünmez taş kaplamalı büyük boy ayna çerçeveleri ve karmaşık heykel tasarımları elde edebiliriz. Ağırlık idare edilebilir seviyelerde tutulabilinirken, toplam materyal kalınlığı da 9–12 mm arasındadır ve gerekli gergi ve bağlantı elemanları çerçeve içine lamine edilmiştir.', '', '', 'mirrors', '', '', '', 0, 0),
(23, 3, 'Seating', '', 'ArmourCast oturma sırası dizaynı üretimi için kullanılabilir, fazla ağırlık yaratmadan taşın anıtsal sağlamlığını verir. Malzeme kalıbın yüzüne paketlenmiş ve 12–15 mm kalınlığında güçlendirilmiş bir yüzey yaratmak için cam elyaf ile birkaç kat lamine edilmiştir. Ahşap yada çelik borunun destek iskeleti daha sonra güçlendirmek için ve ve ayaklar yada tekerlekler için sabitleyici noktalar sağlamak için yerine lamine edilir.', '', '', 'seating', '', '', '', 0, 0),
(24, 3, 'Custom Design', '', 'Arttırılmış doku ve renk seçenekleriyle Armourcoat polished plaster''lar görkemli ve dayanıklı bir yüzeye sahip olmanızı sağlar.', '', '', 'custom-design', '', '', '', 0, 0),
(25, 1, 'Koncrete', '', 'Armourcoat KonCrete® is an urban range of polished plaster colours and finishes designed for contemporary projects. The range offers a wealth of design options to achieve a distinctive modern look, including distressed effects or recessed ‘shutter’ markings. We can match to existing concrete or decorative elements to give you the perfect effect.', '', '', 'koncrete', '', '', '', 0, 0),
(26, 1, 'SMG', '', 'SMG is a super luxury finish with the appearance of a seamless high-gloss ‘lacquer’ effect, with virtually no surface movement visible. The finish works best in black or white as a stunning backdrop for haute couture. SMG provides an opulent decorative effect, equally suited for walls and ceilings, offering great light reflection to create an inviting and spacious interior.', '', '', 'smg', '', '', '', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
