<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->check_isvalidated();
    }

    public function index()
    {
        $this->load->model('msayfa');
        $bilgi['anasayfa'] = $this->msayfa->anasayfa();
        $bilgi['sayfa'] = $this->msayfa->sayfa("8");
        $this->load->view('admin/bas');
        $this->load->view('admin/anasayfa', $bilgi);
        $this->load->view('admin/son');
    }

    public function anasayfakaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $sonuc = $this->do_upload($config);
        $this->load->model('madmin');
        $result = $this->madmin->anasayfakaydet($sonuc);
        redirect(base_url("admin"));
    }

    public function anasayfaedit($sayfa)
    {
        $this->load->model('msayfa');
        $bilgi['sayfa'] = $this->msayfa->noktaatis('anasayfa', 'id', $sayfa);
        $this->load->view('admin/anasayfaedit', $bilgi);
    }

    public function anasayfasil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin"));
    }

    public function hakkimizda()
    {
        $this->load->model('msayfa');
        $bilgi['slider'] = $this->msayfa->slider("sayfa", 1);
        $bilgi['sayfa'] = $this->msayfa->sayfa("1");
        $this->load->view('admin/bas');
        $this->load->view('admin/hakkimizda', $bilgi);
        $this->load->view('admin/son');
    }

    public function hakkimizdakaydet()
    {
        $this->load->model('madmin');
        $result = $this->madmin->sayfakaydet();
        redirect(base_url("admin/hakkimizda"));
    }

    public function hakkimizdaslidersil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/hakkimizda"));
    }

    public function amacimiz()
    {
        $this->load->model('msayfa');
        $bilgi['sayfa'] = $this->msayfa->sayfa("2");
        $bilgi['slider'] = $this->msayfa->slider("sayfa", 2);
        $this->load->view('admin/bas');
        $this->load->view('admin/amacimiz', $bilgi);
        $this->load->view('admin/son');
    }

    public function amacimizkaydet()
    {
        $this->load->model('madmin');
        $result = $this->madmin->sayfakaydet();
        redirect(base_url("admin/amacimiz"));
    }

    public function amacimizslidersil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/amacimiz"));
    }

    public function bizkimiz()
    {
        $this->load->model('msayfa');
        $bilgi['slider'] = $this->msayfa->slider("sayfa", 3);
        $bilgi['sayfa'] = $this->msayfa->sayfa("3");
        $bilgi['ekip'] = $this->msayfa->ekip();
        $this->load->view('admin/bas');
        $this->load->view('admin/bizkimiz', $bilgi);
        $this->load->view('admin/son');
    }

    public function ekipkaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $sonuc = $this->do_upload($config);
        $this->load->model('madmin');
        $result = $this->madmin->ekipkaydet($sonuc);
        redirect(base_url("admin/bizkimiz"));
    }

    public function bizkimizedit($sayfa)
    {
        $this->load->model('msayfa');
        $bilgi['sayfa'] = $this->msayfa->noktaatis('ekip', 'id', $sayfa);
        $this->load->view('admin/bizkimizedit', $bilgi);
    }

    public function ekipsil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/bizkimiz"));
    }

    public function bizkimizslidersil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/bizkimiz"));
    }

    public function haberler()
    {
        $this->load->model('msayfa');
        $bilgi['slider'] = $this->msayfa->slider("sayfa", 4);
        $bilgi['sayfa'] = $this->msayfa->sayfa("4");
        $bilgi['haberler'] = $this->msayfa->haberler();
        $this->load->view('admin/bas');
        $this->load->view('admin/haberler', $bilgi);
        $this->load->view('admin/son');
    }

    public function haberkaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $sonuc = $this->do_upload($config);
        $this->load->model('madmin');
        $result = $this->madmin->haberkaydet($sonuc);
        redirect(base_url("admin/haberler"));
    }

    public function habersil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/haberler"));
    }

    public function haberlerslidersil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/haberler"));
    }

    public function referanslar()
    {
        $this->load->model('msayfa');
        $bilgi['slider'] = $this->msayfa->slider("sayfa", 5);
        $bilgi['sayfa'] = $this->msayfa->sayfa("5");
        $bilgi['haberler'] = $this->msayfa->referanslar();
        $this->load->view('admin/bas');
        $this->load->view('admin/referanslar', $bilgi);
        $this->load->view('admin/son');
    }

    public function referanskaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $_FILES['userfile'] = $_FILES['buyuk'];
        $buyuk = $this->do_upload($config);
        $_FILES['userfile'] = $_FILES['kucuk'];
        $kucuk = $this->do_upload($config);
        $this->load->model('madmin');
        $result = $this->madmin->referanskaydet($buyuk, $kucuk);
        redirect(base_url("admin/referanslar"));
    }

    public function referansedit($sayfa)
    {
        $this->load->model('msayfa');
        $bilgi['sayfa'] = $this->msayfa->noktaatis('referanslar', 'id', $sayfa);
        $this->load->view('admin/referansedit', $bilgi);
    }

    public function referansslidersil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/referanslar"));
    }

    public function referanssil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/referanslar"));
    }

    public function referansupdate()
    {
        $id = $this->input->post('id');
        $deger = $this->input->post('deger');
        $this->db->where("id", $id)->update('referanslar', array('statu' => $deger));
    }

    public function kategoriler()
    {
        $this->load->model('msayfa');
        $bilgi['kategoriler'] = $this->msayfa->kategoriler();
        $this->load->view('admin/bas');
        $this->load->view('admin/kategoriler', $bilgi);
        $this->load->view('admin/son');
    }

    public function kategorikaydet()
    {
        $this->load->model('madmin');
        $result = $this->madmin->kategorikaydet();
        redirect(base_url("admin/kategoriler"));
    }

    public function kategorisil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/kategoriler"));
    }

    public function kategorigaleri($id)
    {
        $this->load->model('msayfa');
        $bilgi['galeri'] = $this->msayfa->galeri("kategori", $id);
        $bilgi['sayfa'] = $this->msayfa->kategori($id);
        $bilgi['id'] = $id;
        $this->load->view('admin/bas');
        $this->load->view('admin/kategorigaleri', $bilgi);
        $this->load->view('admin/son');
    }

    public function kategorigalerikaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $_FILES['userfile'] = $_FILES['kucuk'];
        $kucuk = $this->do_upload($config);
        $_FILES['userfile'] = $_FILES['buyuk'];
        $buyuk = $this->do_upload($config);
        if ($kucuk == "0" && $buyuk == "0") {
            echo "Resim yükleme başarısız.";
        } else {
            $this->load->model('madmin');
            $result = $this->madmin->galerikaydet($buyuk, $kucuk);
        }
        $callback = $this->input->post('callback');
        redirect(base_url("admin/kategorigaleri/" . $callback));
    }

    public function kategorigalerisil()
    {
        $callback = $this->uri->segment(5);
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/kategorigaleri/" . $callback));
    }

    public function slidercek()
    {
        $this->load->model('madmin');
        $result = $this->madmin->slidercek();
        print_r(json_encode($result));
    }

    public function sliderkaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $sonuc = $this->do_upload($config);
        if ($sonuc == "0") {
            echo "Resim yükleme başarısız.";
        } else {
            $this->load->model('madmin');
            $result = $this->madmin->sliderkaydet($sonuc);
        }
        $callback = $this->input->post('callback');
        redirect(base_url("admin/slider/" . $callback));
    }

    public function sayfasliderkaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $sonuc = $this->do_upload($config);
        if ($sonuc == "0") {
            echo "Resim yükleme başarısız.";
        } else {
            $this->load->model('madmin');
            $result = $this->madmin->sliderkaydet($sonuc);
        }
        $callback = $this->input->post('callback');
        redirect(base_url("admin/" . $callback));
    }

    public function sliderguncelle()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $sonuc = $this->do_upload($config);
        $this->load->model('madmin');
        $result = $this->madmin->sliderguncelle($sonuc);
        $callback = $this->input->post('callback');
        redirect(base_url("admin/" . $callback));
    }

    public function slider($id)
    {
        $this->load->model('msayfa');
        $bilgi['galeri'] = $this->msayfa->slider("kategori", $id);
        $bilgi['sayfa'] = $this->msayfa->kategori($id);
        $bilgi['id'] = $id;
        $this->load->view('admin/bas');
        $this->load->view('admin/slider', $bilgi);
        $this->load->view('admin/son');
    }

    public function slidersil()
    {
        $callback = $this->uri->segment(5);
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/slider/" . $callback));
    }

    public function pdf($id)
    {
        $this->load->model('msayfa');
        $bilgi['pdf'] = $this->msayfa->pdf("kategori", $id);
        $bilgi['sayfa'] = $this->msayfa->kategori($id);
        $bilgi['id'] = $id;
        $this->load->view('admin/bas');
        $this->load->view('admin/pdf', $bilgi);
        $this->load->view('admin/son');
    }

    public function pdfkaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'pdf|doc|docx|gif|jpg|png';
        $config['max_size'] = '10000';
        $kucuk = $this->do_upload($config);
        if ($kucuk == "0") {
            echo "Resim yükleme başarısız.";
        } else {
            $this->load->model('madmin');
            $result = $this->madmin->pdfkaydet($kucuk);
        }
        $callback = $this->input->post('callback');
        redirect(base_url("admin/pdf/" . $callback));
    }

    public function pdfsil()
    {
        $callback = $this->uri->segment(5);
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/pdf/" . $callback));
    }

    public function urunler()
    {
        $this->load->model('msayfa');
        $bilgi['urunler'] = $this->msayfa->urunler();
        $this->load->view('admin/bas');
        $this->load->view('admin/urunler', $bilgi);
        $this->load->view('admin/son');
    }

    public function urunkaydet()
    {
        $this->load->model('madmin');
        $result = $this->madmin->urunkaydet();
        redirect(base_url("admin/urunler"));
    }

    public function urunsil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/urunler"));
    }

    public function urungaleri($id)
    {
        $this->load->model('msayfa');
        $bilgi['galeri'] = $this->msayfa->galeri("urungaleri", $id);
        $bilgi['sayfa'] = $this->msayfa->urun($id);
        $bilgi['id'] = $id;
        $this->load->view('admin/bas');
        $this->load->view('admin/urungaleri', $bilgi);
        $this->load->view('admin/son');
    }

    public function urungalerikaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $_FILES['userfile'] = $_FILES['kucuk'];
        $kucuk = $this->do_upload($config);
        $_FILES['userfile'] = $_FILES['buyuk'];
        $buyuk = $this->do_upload($config);
        if ($kucuk == "0" && $buyuk == "0") {
            echo "Resim yükleme başarısız.";
        } else {
            $this->load->model('madmin');
            $result = $this->madmin->galerikaydet($buyuk, $kucuk);
        }
        $callback = $this->input->post('callback');
        redirect(base_url("admin/urungaleri/" . $callback));
    }

    public function urungalerisil()
    {
        $callback = $this->uri->segment(5);
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/urungaleri/" . $callback));
    }

    public function urunpalet($id)
    {
        $this->load->model('msayfa');
        $bilgi['palet'] = $this->msayfa->galeri("urunpalet", $id);
        $bilgi['sayfa'] = $this->msayfa->urun($id);
        $bilgi['id'] = $id;
        $this->load->view('admin/bas');
        $this->load->view('admin/urunpalet', $bilgi);
        $this->load->view('admin/son');
    }

    public function nitelikedit($id)
    {
        $this->load->model('msayfa');
        $bilgi['sayfa'] = $this->msayfa->urun($id);
        $bilgi['id'] = $id;
        $this->load->view('admin/bas');
        $this->load->view('admin/nitelikedit', $bilgi);
        $this->load->view('admin/son');
    }

    public function nitelikkaydet()
    {
        $id = $this->input->post('id');
        $deger = json_encode($this->input->post('nitelik'));
        $this->db->where("id", $id)->update("urunler", array("t04" => $deger));
        redirect(base_url("admin/nitelikedit/" . $id));


    }

    public function urunpaletkaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $_FILES['userfile'] = $_FILES['kucuk'];
        $kucuk = $this->do_upload($config);
        $_FILES['userfile'] = $_FILES['buyuk'];
        $buyuk = $this->do_upload($config);
        if ($kucuk == "0" && $buyuk == "0") {
            echo "Resim yükleme başarısız.";
        } else {
            $this->load->model('madmin');
            $result = $this->madmin->paletkaydet($buyuk, $kucuk);
        }
        $callback = $this->input->post('callback');
        redirect(base_url("admin/urunpalet/" . $callback));
    }

    public function urunpaletsil()
    {
        $callback = $this->uri->segment(5);
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/urunpalet/" . $callback));
    }

    public function urunslidercek()
    {
        $this->load->model('madmin');
        $result = $this->madmin->slidercek();
        print_r(json_encode($result));
    }

    public function urunsliderkaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $sonuc = $this->do_upload($config);
        if ($sonuc == "0") {
            echo "Resim yükleme başarısız.";
        } else {
            $this->load->model('madmin');
            $result = $this->madmin->sliderkaydet($sonuc);
        }
        $callback = $this->input->post('callback');
        redirect(base_url("admin/urunslider/" . $callback));
    }

    public function urunsliderguncelle()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $sonuc = $this->do_upload($config);
        $this->load->model('madmin');
        $result = $this->madmin->sliderguncelle($sonuc);
        $callback = $this->input->post('callback');
        redirect(base_url("admin/urunslider/" . $callback));
    }

    public function urunslider($id)
    {
        $this->load->model('msayfa');
        $bilgi['galeri'] = $this->msayfa->slider("urun", $id);
        $bilgi['sayfa'] = $this->msayfa->urun($id);
        $bilgi['id'] = $id;
        $this->load->view('admin/bas');
        $this->load->view('admin/urunslider', $bilgi);
        $this->load->view('admin/son');
    }

    public function urunslidersil()
    {
        $callback = $this->uri->segment(5);
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/urunslider/" . $callback));
    }

    public function urunpdf($id)
    {
        $this->load->model('msayfa');
        $bilgi['pdf'] = $this->msayfa->pdf("urun", $id);
        $bilgi['sayfa'] = $this->msayfa->urun($id);
        $bilgi['id'] = $id;
        $this->load->view('admin/bas');
        $this->load->view('admin/urunpdf', $bilgi);
        $this->load->view('admin/son');
    }

    public function urunpdfkaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'pdf|doc|docx|gif|jpg|png';
        $config['max_size'] = '10000';
        $kucuk = $this->do_upload($config);
        if ($kucuk == "0") {
            echo "Resim yükleme başarısız.";
        } else {
            $this->load->model('madmin');
            $result = $this->madmin->pdfkaydet($kucuk);
        }
        $callback = $this->input->post('callback');
        redirect(base_url("admin/urunpdf/" . $callback));
    }

    public function urunpdfsil()
    {
        $callback = $this->uri->segment(5);
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/urunpdf/" . $callback));
    }

    public function seo()
    {
        $this->load->model('madmin');
        $result = $this->madmin->seo();
        $callback = $this->input->post('callback');
        redirect(base_url("admin/" . $callback));
    }

    public function bg()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $sonuc = $this->do_upload($config);
        if ($sonuc == "0") {
            echo "Resim yükleme başarısız.";
        } else {
            $this->load->model('madmin');
            $result = $this->madmin->bg($sonuc);
        }
        $callback = $this->input->post('callback');
        redirect(base_url("admin/" . $callback));
    }

    public function cek()
    {
        $this->load->model('madmin');
        $result = $this->madmin->cek();
        print_r(json_encode($result));
    }

    public function galeri()
    {
        $bilgi['sayfa'] = $this->db->get('galeri');
        $this->load->view('admin/bas');
        $this->load->view('admin/galeri', $bilgi);
        $this->load->view('admin/son');
    }

    public function galerikaydet()
    {
        $this->load->model('madmin');
        $result = $this->madmin->mgalerikaydet();
        redirect(base_url("admin/galeri"));
    }

    public function galerisil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/galeri"));
    }

    public function galeriresimler($id)
    {
        $this->load->model('msayfa');
        $bilgi['galeri'] = $this->msayfa->galeri("genelgaleri", $id);
        $bilgi['sayfa'] = $this->db->where("id", $id)->get('galeri')->row();
        $bilgi['id'] = $id;
        $this->load->view('admin/bas');
        $this->load->view('admin/galeriresimler', $bilgi);
        $this->load->view('admin/son');
    }

    public function galeriresimkaydet()
    {
        $config['upload_path'] = './photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $_FILES['userfile'] = $_FILES['kucuk'];
        $kucuk = $this->do_upload($config);
        $_FILES['userfile'] = $_FILES['buyuk'];
        $buyuk = $this->do_upload($config);
        if ($kucuk == "0" && $buyuk == "0") {

        } else {
            $this->load->model('madmin');
            $result = $this->madmin->galerikaydet($buyuk, $kucuk);
        }
        $callback = $this->input->post('callback');
        redirect(base_url("admin/galeriresimler/" . $callback));
    }

    public function galeriresimsil()
    {
        $callback = $this->uri->segment(5);
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/galeriresimler/" . $callback));
    }


    public function teknik()
    {
        $bilgi['sayfa'] = $this->db->get('teknik');
        $this->load->view('admin/bas');
        $this->load->view('admin/teknik', $bilgi);
        $this->load->view('admin/son');
    }

    public function teknikkaydet()
    {
        $this->load->model('madmin');
        $result = $this->madmin->mteknikkaydet();
        redirect(base_url("admin/teknik"));
    }

    public function tekniksil()
    {
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/teknik"));
    }

    public function teknikdokumanlar($id)
    {
        $this->load->model('msayfa');
        $bilgi['pdf'] = $this->msayfa->pdf("teknik", $id);
        $bilgi['sayfa'] = $this->db->where("id", $id)->get('teknik')->row();
        $bilgi['id'] = $id;
        $this->load->view('admin/bas');
        $this->load->view('admin/teknikdokumanlar', $bilgi);
        $this->load->view('admin/son');
    }

    public function teknikdokumankaydet()
    {
        //$config['upload_path'] = './photos/';
        //$config['allowed_types'] = 'pdf|doc|docx|gif|jpg|png';
        //$config['max_size']	= '10000';
        //$kucuk=$this->do_upload($config);
        $this->load->model('madmin');
        //$result = $this->madmin->pdfkaydet($kucuk);

        $result = $this->madmin->mteknikdokumankaydet();
        $callback = $this->input->post('callback');
        redirect(base_url("admin/teknikdokumanlar/" . $callback));
    }

    public function teknikdokumansil()
    {
        $callback = $this->uri->segment(5);
        $id = $this->uri->segment(4);
        $tbl = $this->uri->segment(3);
        $this->load->model('madmin');
        $result = $this->madmin->sil($tbl, $id);
        redirect(base_url("admin/teknikdokumanlar/" . $callback));
    }

    public function paletcodeupdate()
    {
        $id = $this->input->post('id');
        $deger = $this->input->post('deger');
        $this->db->where("id", $id)->update("photos", array("t03" => $deger));
    }

    public function do_logout()
    {
        $this->session->sess_destroy();
        redirect(base_url('signup'));
    }

    private function check_isvalidated()
    {
        if (!$this->session->userdata('validated')) {
            redirect(base_url('signup'));
        }
    }

    function do_upload($config)
    {
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $sonuc = 0;
            return $sonuc;
        } else {
            $data = array('upload_data' => $this->upload->data());
            $sonuc = $data['upload_data']['file_name'];
            return $sonuc;
        }
    }

    public function logout()
    {
        $data = array('sid' => "", 'sops' => "", 'yetki' => "", 'validated' => "",);
        $this->session->unset_userdata($data);
        js_redirect(base_url("admin/"));
    }
}