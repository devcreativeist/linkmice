<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller
{
    public function index()
    {
        $this->load->model('msayfa');
        $bilgi['slider'] = $this->db->get("anasayfa")->result();
        $bilgi['anasayfa'] = $this->msayfa->anasayfa();
        $bilgi['hakkimizda'] = $this->db->where("id", 1)->get("sayfa")->row();
        $bilgi['whatWeDoList'] = $this->db->get("referanslar")->result();
        $bilgi['newsList'] = $this->db->get("haberler")->result();
        $bilgi['teamList'] = $this->db->get("ekip")->result();
        $bilgi['workWithUs'] =  $this->db->where("id", 2)->get("sayfa")->row();
        $bilgi['photos'] = $this->db->where(array("aid"=>"genelgaleri","bid"=>1))->get("photos")->result();
        $bilgi['videos'] = $this->db->where(array("aid"=>"genelgaleri","bid"=>2))->get("photos")->result();
        $bilgi['references'] = $this->db->where(array("aid"=>"genelgaleri","bid"=>3))->get("photos")->result();

        $this->load->view('anasayfa', $bilgi);
    }
    public function map()
    {


        $this->load->view('map');
    }
}