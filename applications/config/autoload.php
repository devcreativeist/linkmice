<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$autoload['packages'] = array();
$autoload['libraries'] = array('database','session');
$autoload['helper'] = array('url','js_redirect','text');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array();