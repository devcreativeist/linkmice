<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$route['default_controller'] = "welcome";
$route['404_override'] = '';

$route['admin'] = "admin/index";
$route['admin/(.*)'] = "admin/$1";

$route['resize'] = "resize/index";
$route['resize/(.*)'] = "resize/$1";

$route['signup'] = "signup/index";
$route['signup/(.*)'] = "signup/$1";

$route['^(.*).html'] = "welcome/$1";
