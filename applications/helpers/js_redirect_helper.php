<?php
    function js_redirect( $link ,$https = ''){
        if(!empty($https)){
            ?>
                <script type="text/javascript">window.location.href="<?php echo str_replace('http','https',$link); ?>";</script>
            <?php
        } else {
            ?>
                <script type="text/javascript">window.location.href="<?php echo $link; ?>";</script>
            <?php
        }
    }
?>
