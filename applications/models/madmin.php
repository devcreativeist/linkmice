<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Madmin extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function makeseo($kelime)
    {
        $turkce = array('!', '?', ':', '&rsquo;', '.', '/', '+', ' ', 'ç', 'Ç', 'ğ', 'Ğ', 'ü', 'Ü', 'ö', 'Ö', 'ı', 'İ', 'ş', 'Ş', '(', ')');
        $utf8 = array('', '', '', '', '', '-', '', '-', 'c', 'c', 'g', 'g', 'u', 'u', 'o', 'o', 'i', 'i', 's', 's', '', '');
        $seolnk = strtolower(str_replace($turkce, $utf8, $kelime));
        return $seolnk;
    }

    public function anasayfakaydet($sonuc)
    {
        $id = $this->input->post('id');
        $isim = $this->input->post('isim');
        $mekan = $this->input->post('mekan');
        $aciklama = $this->input->post('aciklama');
        $url = $this->input->post('url');
        $kategori = $this->input->post('kategori');
        $data = array("isim" => $isim, "mekan" => $mekan, "aciklama" => $aciklama, "kategori" => $kategori, "resim" => $sonuc);
        if (empty($id)) {
            $query = $this->db->insert("anasayfa", $data);
        } else {
            $this->db->where('id', $id);
            if ($sonuc == "0") {
                $data = array("isim" => $isim, "mekan" => $mekan, "url" => $url, "aciklama" => $aciklama, "kategori" => $kategori);
            } else {
                $data = array("isim" => $isim, "mekan" => $mekan, "aciklama" => $aciklama, "resim" => $sonuc);
            }
            $query = $this->db->update("anasayfa", $data);
        }
        return $query;
    }

    public function sayfakaydet()
    {
        $id = $this->input->post('id');
        $baslik = $this->input->post('baslik');
        $detay = $this->input->post('detay');
        $data = array("t01" => $baslik, "t02" => $detay);
        $this->db->where('id', $id);
        $query = $this->db->update("sayfa", $data);
        return $query;
    }

    public function seo()
    {
        $id = $this->input->post('id');
        $title = $this->input->post('title');
        $desc = $this->input->post('desc');
        $keys = $this->input->post('keys');
        $tbl = $this->input->post('tbl');
        $data = array("title" => $title, "desc" => $desc, "keys" => $keys);
        $this->db->where('id', $id);
        $query = $this->db->update($tbl, $data);
        return $query;
    }

    public function bg($sonuc)
    {
        $id = $this->input->post('id');
        $tbl = $this->input->post('tbl');
        $data = array("bg" => $sonuc);
        $this->db->where('id', $id);
        $query = $this->db->update($tbl, $data);
        return $query;
    }

    public function ekipkaydet($sonuc)
    {
        $id = $this->input->post('id');
        $t01 = $this->input->post('t01');
        $t02 = $this->input->post('t02');
        $t03 = $this->input->post('t03');
        $t04 = $this->input->post('t04');
        if ($sonuc != "0") {
            $data = array("t01" => $t01, "t02" => $t02, "t03" => $t03, "t04" => $t04, "t05" => $sonuc);
        }else{
            $data = array("t01" => $t01, "t02" => $t02, "t03" => $t03, "t04" => $t04);
        }
        if (empty($id)) {
            $query = $this->db->insert("ekip", $data);
        } else {
            $this->db->where('id', $id);
            if ($sonuc == "0") {
                $data = array("t01" => $t01, "t02" => $t02, "t03" => $t03, "t04" => $t04);
            } else {
                $data = array("t01" => $t01, "t02" => $t02, "t03" => $t03, "t04" => $t04, "t05" => $sonuc);
            }
            $query = $this->db->update("ekip", $data);
        }
        return $query;
    }

    public function haberkaydet($sonuc)
    {
        $id = $this->input->post('id');
        $t01 = $this->input->post('t01');
        $t02 = $this->input->post('t02');
        $t03 = $this->input->post('t03');
        $data = array("t01" => $t01, "t02" => $t02, "t03" => $t03, "t05"=>$sonuc);
        if (empty($id)) {
            $query = $this->db->insert("haberler", $data);
        } else {
            $this->db->where('id', $id);
            $query = $this->db->update("haberler", $data);
        }
        return $query;
    }

    public function kategorikaydet()
    {
        $id = $this->input->post('id');
        $t01 = $this->input->post('t01');
        $t03 = $this->input->post('t03');
        $seo = $this->makeseo($t01);
        $data = array("t01" => $t01, "t03" => $t03, "seo" => $seo);
        if (empty($id)) {
            $query = $this->db->insert("kategoriler", $data);
        } else {
            $this->db->where('id', $id);
            $query = $this->db->update("kategoriler", $data);
        }
        return $query;
    }

    public function urunkaydet()
    {
        $id = $this->input->post('id');
        $aid = $this->input->post('aid');
        $t01 = $this->input->post('t01');
        $t03 = $this->input->post('t03');
        $seo = $this->makeseo($t01);
        $data = array("aid" => $aid, "t01" => $t01, "t03" => $t03, "seo" => $seo);
        if (empty($id)) {
            $query = $this->db->insert("urunler", $data);
        } else {
            $this->db->where('id', $id);
            $query = $this->db->update("urunler", $data);
        }
        return $query;
    }

    public function referanskaydet($buyuk, $kucuk)
    {
        $id = $this->input->post('id');
        $t01 = $this->input->post('t01');
        $t02 = $this->input->post('t02');
        $t03 = $this->input->post('t03');
        $data = array("t01" => $t01, "t02" => $t02, "t03" => $t03, "t04" => $buyuk, "t05" => $kucuk);
        if (empty($id)) {
            $query = $this->db->insert("referanslar", $data);
        } else {
            $this->db->where('id', $id);
            if ($buyuk == "0" && $kucuk == "0") {
                $data = array("t01" => $t01, "t02" => $t02, "t03" => $t03);
                $query = $this->db->update("referanslar", $data);
            }
            if ($buyuk != "0") {
                $data = array("t04" => $buyuk);
                $query = $this->db->update("referanslar", $data);
            }
            if ($kucuk != "0") {
                $data = array("t05" => $kucuk);
                $query = $this->db->update("referanslar", $data);
            }
        }
        return $query;
    }

    public function sil($tbl, $id)
    {
        $this->db->where('id', $id);
        $query = $this->db->delete($tbl);
        return $query;
    }

    public function cek()
    {
        $id = $this->input->post('id');
        $tbl = $this->input->post('tablo');
        $this->db->where('id', $id);
        $query = $this->db->get($tbl)->row();
        return $query;
    }

    public function sliderkaydet($fname)
    {
        $aid = $this->input->post('tbl');
        $bid = $this->input->post('id');
        $t02 = $this->input->post('t02');
        $t03 = $this->input->post('t03');
        $kategori = $this->input->post('kategori');
        $data = array("aid" => $aid, "bid" => $bid, "t01" => $fname, "t02" => $t02, "t03" => $t03, "t04" => $kategori);
        $query = $this->db->insert("slider", $data);
        return $query;
    }

    public function galerikaydet($buyuk, $kucuk)
    {
        $aid = $this->input->post('tbl');
        $bid = $this->input->post('id');
        $t03 = $this->input->post('t03');
        $t04 = $this->input->post('t04');
        $data = array("aid" => $aid, "bid" => $bid, "t01" => $kucuk, "t02" => $buyuk, "t03" => $t03, "t04" => $t04);
        $query = $this->db->insert("photos", $data);
        return $query;
    }

    public function paletkaydet($buyuk, $kucuk)
    {
        $aid = $this->input->post('tbl');
        $bid = $this->input->post('id');
        $t03 = $this->input->post('t03');
        $t04 = $this->input->post('t04');
        $data = array("aid" => $aid, "bid" => $bid, "t01" => $kucuk, "t02" => $buyuk, "t03" => $t03, "t04" => $t04);
        $query = $this->db->insert("photos", $data);
        return $query;
    }

    public function pdfkaydet($kucuk)
    {
        $aid = $this->input->post('tbl');
        $bid = $this->input->post('id');
        $data = array("aid" => $aid, "bid" => $bid, "t01" => $kucuk);
        $query = $this->db->insert("pdf", $data);
        return $query;
    }

    public function sliderguncelle($fname)
    {
        $id = $this->input->post('sid');
        $t02 = json_encode($this->input->post('t02'));
        $t03 = json_encode($this->input->post('t03'));
        if ($fname == "0") {
            $data = array("t02" => $t02, "t03" => $t03);
        } else {
            $data = array("t01" => $fname, "t02" => $t02, "t03" => $t03);
        }
        $this->db->where('id', $id);
        $query = $this->db->update("slider", $data);
        return $query;
    }

    public function slidercek()
    {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $query = $this->db->get("slider")->row();
        return $query;
    }

    public function mgalerikaydet()
    {
        $id = $this->input->post('id');
        $t01 = $this->input->post('t01');
        $data = array("t01" => $t01);
        if (empty($id)) {
            $query = $this->db->insert("galeri", $data);
        } else {
            $this->db->where('id', $id);
            $query = $this->db->update("galeri", $data);
        }
        return $query;
    }

    public function mteknikkaydet()
    {
        $id = $this->input->post('id');
        $t01 = $this->input->post('t01');
        $data = array("t01" => $t01);
        if (empty($id)) {
            $query = $this->db->insert("teknik", $data);
        } else {
            $this->db->where('id', $id);
            $query = $this->db->update("teknik", $data);
        }
        return $query;
    }

    public function mteknikdokumankaydet()
    {
        $id = $this->input->post('id');
        $aid = $this->input->post('tbl');
        $bid = $this->input->post('bid');
        $t01 = $this->input->post('t01');
        $t02 = $this->input->post('t02');
        $t03 = $this->input->post('t03');
        $data = array("aid" => $aid, "bid" => $bid, "t01" => $t01, "t02" => $t02, "t03" => $t03);
        if (empty($id)) {
            $query = $this->db->insert("pdf", $data);
        } else {
            $this->db->where('id', $id);
            $query = $this->db->update("teknik", $data);
        }
        return $query;
    }

}