<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Msayfa extends CI_Model{function __construct(){parent::__construct();}
	public function anasayfa(){$query = $this->db->get('anasayfa');return $query;}
	public function noktaatis($tbl,$stn,$val){$query=$this->db->where($stn, $val)->get($tbl)->row();return $query;}
	public function ekip(){$query = $this->db->order_by("jum")->get('ekip');return $query;}
	public function haberler(){$query = $this->db->get('haberler');return $query;}
	public function kategoriler(){$query = $this->db->get('kategoriler');return $query;}
	public function urunler(){$query = $this->db->get('urunler');return $query;}
	public function urun($id){$this->db->where('id',$id);$query = $this->db->get('urunler')->row();return $query;}
	public function kategori($id){$this->db->where('id',$id);$query = $this->db->get('kategoriler')->row();return $query;}
	public function kategoriseo($seo){$this->db->where('seo',$seo);$query = $this->db->get('kategoriler')->row();return $query;}
	public function urunseo($seo){$this->db->where('seo',$seo);$query = $this->db->get('urunler')->row();return $query;}
	public function referanslar(){$query = $this->db->get('referanslar');return $query;}
	public function sayfa($id){$this->db->where('id',$id);$query = $this->db->get('sayfa')->row();return $query;}
	public function cek($tbl,$id){$this->db->where('id', $id);$query = $this->db->get($tbl)->row();return $query;}
	public function slider($tbl,$id){$data=array("aid"=>$tbl, "bid"=>$id);$this->db->where($data);$query = $this->db->get('slider');return $query;}
	public function galeri ($tbl,$id){$data=array("aid"=>$tbl, "bid"=>$id);$this->db->where($data);$query = $this->db->get('photos');return $query;}
	public function pdf ($tbl,$id){$data=array("aid"=>$tbl, "bid"=>$id);$this->db->where($data);$query = $this->db->get('pdf');return $query;}
}

?>