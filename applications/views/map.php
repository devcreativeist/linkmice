

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Google Map</title>
</head>

<body style="padding: 0px; margin: 0px; overflow: hidden;">

    <div id="map_canvas" style="width: 960px; height: 274px; overflow: hidden;"></div>

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        function initialize() {
            var Koordinatlar = new google.maps.LatLng(41.080444, 29.037226);
            var myOptions = {
                zoom: 17,
                center: Koordinatlar,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            var BilgiPenceresi = new google.maps.InfoWindow(
                {
                    content: '<font size=2><br />Etiler Mh. Bıyıklı Mehmet Paşa Sk.<br> No:11/2 Çamlık Apt. <br>Beşiktaş/İstanbul<br>Tel: +90 2122632001</font>',
                    size: new google.maps.Size(50, 50),
                    position: Koordinatlar
                });
            //BilgiPenceresi.open(map);

            var Isagretci = new google.maps.Marker({
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: Koordinatlar,
                icon: 'images/map_icon.png'
            });
            google.maps.event.addListener(Isagretci, 'dragend', function () {
                Isagretci.setAnimation(google.maps.Animation.BOUNCE);
            });
        }
</script>
    <script type="text/javascript">    initialize();</script>
</body>
</html>
