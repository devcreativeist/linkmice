<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="copyright" content="www.linkmice.com" />
<meta name="classification" content="Business" />
<meta name="distribution" content="Global" />
<meta name="content-language" content="tr" />
<meta name="rating" content="All" />
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="2 days" />
<link rel="shortcut icon" href="images/favicon.ico" />
<title>Link Mice</title>
<link rel="stylesheet" type="text/css" href="css/linkmice.css" />
<link rel="stylesheet" type="text/css" href="css/font.css" />
<script type="text/javascript" src="script/jquery.js"></script>
<script type="text/javascript" src="script/main-functions.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />
<script type="text/javascript" src="script/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="script/jquery.fancybox.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.mainMenu li a:eq(3)').addClass('active');
		$('.fancybox').fancybox();
    });
</script>
</head>

<body>
<div class="mainContainer">
  <div class="topContainer">
    <div class="headerCon"> <a href="index.html" title="Link Mice" class="logo"></a>
      <ul class="mainMenu">
        <li><a href="index.html#about-us" title="ABOUT US">ABOUT US</a></li>
        <li><a href="" title="WHAT WE DO?">WHAT WE DO?</a></li>
        <li><a href="" title="WHAT WE DID?">WHAT WE DID?</a></li>
        <li><a href="" title="NEWS">NEWS</a></li>
        <li><a href="" title="OUR TEAM">OUR TEAM</a></li>
        <li><a href="" title="CAREER">CAREER</a></li>
        <li><a href="" title="REFERENCES">REFERENCES</a></li>
        <li><a href="" title="CONTACT">CONTACT</a></li>
      </ul>
    </div>
  </div>
  <div class="middleContainer">
    <div class="pageContainer" id="newsDetail">
      <div class="pageContainerTop"></div>
      <div class="pageContentArea">
        <h1 class="pageTitle">NEWS</h1>
        <h5 class="newsTitle">Lorem Ipsum Dolor Sit Amet</h5>
        <a class="go-back" title="Go Back" href="#" onClick="history.go(-1)">Go Back</a>
        <div class="realContent">
          <div class="newsDetailBox">
            <div class="newsImg"><a href="images/news/1.jpg" rel='photos' class="fancybox" title=""><img src="images/news/1.jpg" width="220" height="220" alt="ornek" /></a></div>
            <div class="newsTxt">
              <div class="newsDate">22.01.2014</div>
              <div class="newsSpot">
                <p>Dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              </div>
              <h4>Lorem Ipsum Dolor Sit Amet</h4>
              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
              <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
               <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
            </div>
          </div>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="footerContainer">
    <div class="footerContent">
      <div class="footerTop"> <span>Newsletters can sign up for our E-Mail Group.</span>
        <form id="" name="" action="">
          <input type="email" id="newsletter" name="newsletter" placeholder="E-Mail Address" required="required" />
          <input type="submit" id="" name="" value="Send" class="sendMail" />
        </form>
      </div>
      <ul class="footerLogos">
        <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/1.jpg" alt="" /></a></li>
        <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/2.jpg" alt="" /></a></li>
        <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/3.jpg" alt="" /></a></li>
        <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/4.jpg" alt="" /></a></li>
        <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/5.jpg" alt="" /></a></li>
        <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/6.jpg" alt="" /></a></li>
      </ul>
    </div>
    <div class="clr"></div>
  </div>
  <a href="javascript:;" class="go-top" title="Go To Top">Go To Top</a> </div>
</body>
</html>
