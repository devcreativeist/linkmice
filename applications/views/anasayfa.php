<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="description" content=""/>
    <meta name="copyright" content="www.linkmice.com"/>
    <meta name="classification" content="Business"/>
    <meta name="distribution" content="Global"/>
    <meta name="content-language" content="tr"/>
    <meta name="rating" content="All"/>
    <meta name="robots" content="index, follow"/>
    <meta name="revisit-after" content="2 days"/>
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <title>Link Mice</title>
    <link rel="stylesheet" type="text/css" href="css/linkmice.css"/>
    <link rel="stylesheet" type="text/css" href="css/font.css"/>
    <script type="text/javascript" src="script/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bjqs.css">
    <script src="script/bjqs-1.3.js"></script>
    <link rel="stylesheet" href="css/ui.tabs.css" type="text/css" media="print, projection, screen"/>
    <script src="script/ui.tabs.js" type="text/javascript"></script>
    <script src="script/main-functions.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen"/>
    <script type="text/javascript" src="script/jquery.mousewheel-3.0.6.pack.js"></script>
    <script type="text/javascript" src="script/jquery.fancybox.js"></script>
    <script type="text/javascript" src="script/jquery.fancybox-media.js?v=1.0.6"></script>
    <link rel="stylesheet" href="css/sinister.css"/>
    <link rel="stylesheet" href="css/animate.min.css"/>
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
    <script>
        $(document).ready(function (e) {
            $('#tabcontainer ul').tabs({fxFade: true, fxSpeed: 'fast'});
            $("#tabcontainer").delegate("a", "click", function () {
                window.location.hash = $(this).attr("href");
                return false;
            });
            $('.go-top').fadeOut();
            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('.go-top').fadeIn();
                } else {
                    $('.go-top').fadeOut();
                }
            });
        });
    </script>
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen"/>
    <script defer src="script/jquery.flexslider.js"></script>
    <script src="script/jquery.themepunch.plugins.min.js"></script>
    <script src="script/jquery.themepunch.revolution.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/settings.css" media="screen"/>
    <link rel="stylesheet" href="css/owl.carousel.css">
    <script src="script/owl.carousel.min.js"></script>
    <script>

        $(document).ready(function () {
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: true,
                slideshow: false,
                directionNav: false,
                itemWidth: 100,
                itemMargin: 5,
                asNavFor: '#slider',
                prevText: "",
                nextText: ""
            });

            $('#slider').flexslider({
                animation: "slide",
                controlNav: true,
                animationLoop: true,
                slideshow: false,
                sync: "#carousel",
                prevText: "",
                nextText: ""
            });

            $('.owl-carousel').on('changed.owl.carousel', function (event) {

            })

        });
    </script>
</head>

<body>
<div class="mainContainer" id="top">
<div class="topContainer">
    <div class="headerCon">
        <a href="index.html" title="Link Mice" class="logo"></a>
        <ul class="mainMenu">
            <li><a href="#about-us" title="ABOUT US">ABOUT US</a></li>
            <li><a href="#what-we-do" title="WHAT WE DO?">WHAT WE DO?</a></li>
            <li><a href="#what-we-did" title="WHAT WE DID?">WHAT WE DID?</a></li>
            <li><a href="#news" title="NEWS">NEWS</a></li>
            <li><a href="#our-team" title="OUR TEAM">OUR TEAM</a></li>
            <li><a href="#career" title="CAREER">CAREER</a></li>
            <li><a href="#references" title="REFERENCES">REFERENCES</a></li>
            <li><a href="#contact" title="CONTACT">CONTACT</a></li>
        </ul>
    </div>
    <a href="#top" class="go-top" title="Go To Top">Go To Top</a>
</div>
<div class="middleContainer">
<section id="Home">
    <div class="fullwidthbanner-container">
        <div class="fullwidthbanner">
            <ul>
                <!-- SLIDE  -->
                <?php foreach($slider as $slide){?>
                <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                    <!-- MAIN IMAGE -->
                    <img src="<?=base_url("photos/".$slide->resim)?>" alt="MEETINGS / " data-bgfit="cover"
                         data-bgposition="center center" data-bgrepeat="no-repeat">
                    <!-- LAYER NR.1 TITLE -->
                    <div class="tp-caption scala_title sft"
                         data-x="center"
                         data-y="center"
                         data-speed="500"
                         data-start="800"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="off"></div>
                </li>
                <?php } ?>

                <!-- SLIDE END  -->
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</section>

<div class="socialMedya">
    <a href="javascript:;" title="facebook" class="facebook" target="_blank"></a>
    <a href="javascript:;" title="twitter" class="twitter" target="_blank"></a>
    <a href="javascript:;" title="instagram" class="instagram" target="_blank"></a>
    <a href="javascript:;" title="youtube" class="youtube" target="_blank"></a>
</div>
<div class="aboutUsCon" id="about-us">
    <h1 class="aboutUsTitle"><?= $hakkimizda->t01 ?></h1>
    <?= $hakkimizda->t02 ?>
</div>
<div class="pageContainer" id="what-we-do">
    <div class="pageContainerTop"></div>
    <div class="pageContentArea">
        <h1 class="pageTitle">WHAT WE DO?</h1>

        <div class="realContent">
            <div id="tabcontainer">
                <ul>
                    <?php
                    foreach ($whatWeDoList as $whatWeDo) {
                        ?>
                        <li><a href="#tab<?= $whatWeDo->id ?>" title="MEETINGS"><?= $whatWeDo->t01 ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="tabContent">
                <?php
                foreach ($whatWeDoList as $whatWeDo) {
                    ?>
                    <div id="tab<?= $whatWeDo->id ?>">
                        <div class="homeWorksContent">
                            <h2 class="subtitle"><?= $whatWeDo->t01 ?></h2>

                            <div class="scrlContent mCustomScrollbar">

                                <?= $whatWeDo->t03 ?>



                            </div>
                        </div>

                        <div class="homeWorksImg"><a class="fancybox" href="images/works/big/1.jpg"
                                                     title="Lorem Ipsum Brand Name 23.11.14" rel="meetings"><img
                                    src="images/works/1.jpg" width="329" height="366" alt="ornek"/></a><a
                                class="fancybox" href="images/works/big/2.jpg" title="Lorem Ipsum Brand Name 23.11.14"
                                rel="meetings"></a></div>
                    </div>
                <?php } ?>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</div>
<div class="pageContainer" id="what-we-did">
    <div class="pageContainerTop"></div>
    <div class="pageContentArea">
        <h1 class="pageTitle">WHAT WE DİD?</h1>

        <div class="realContent">
            <div class="galleryCon" id="photoGallery">
                <div class="galleryTitle">PHOTO ALBUMS</div>


                <?php foreach ($photos as $photo) {?>
                <div class="galleryBox">
                    <div class="ImageWrapper">
                        <a href="images/what-we-did/1.jpg" rel='photos' class="fancybox" title="">
                            <img class="img-responsive" src="<?=base_url("photos/".$photo->t01)?>" width="300" height="300"
                                 alt="ornek"/>

                            <div class="PStyleBe">
                                <div class="allPhotos">ALL PHOTOS</div>
                            </div>
                        </a>
                    </div>
                    <div class="galleryText"><?=$photo->t03?></div>
                </div>
                <?php }?>




            </div>
            <a class="moreContent" href="javascript:;">Load More “What We Did?” Photos +</a>

            <div class="clr"></div>
            <div class="galleryCon" id="videoGallery">
                <div class="galleryTitle">VIDEO ALBUMS</div>
                <?php
                foreach($videos as $video) {
                ?>
                <div class="galleryBox">
                    <div class="ImageWrapper">
                        <a href="<?=$video->t03?>" rel='videos' class="fancybox-media"
                           title="">
                            <img class="img-responsive" src="<?=base_url("photos/".$video->t01)?>" width="300" height="300"
                                 alt="ornek"/>

                            <div class="PStyleBe">
                                <div class="allPhotos">ALL VIDEOS</div>
                            </div>
                        </a>
                    </div>
                    <div class="galleryText"></div>
                </div>
                <?php }?>

            </div>
            <a class="moreContent" href="javascript:;">Load More “What We Did?” Videos +</a>
        </div>
    </div>
    <div class="clr"></div>
</div>
<div class="pageContainer" id="news">
    <div class="pageContainerTop"></div>
    <div class="pageContentArea">
        <h1 class="pageTitle">NEWS</h1>

        <div class="realContent">
            <?php foreach ($newsList as $news) { ?>
                <div class="newsBox">
                    <div class="newsImg"><img src="<?=base_url("photos/".$news->t05)?>" width="220" height="220" alt="ornek"/></div>
                    <div class="newsTxt">
                        <h4><?= $news->t01 ?></h4>
                        <p><?= $news->t02 ?></p>
                        <a class="more" title="Read More" href="haber-detay.html">Read More +</a>
                    </div>
                </div>
            <?php } ?>
            <a class="moreContent" href="javascript:;">Load More News +</a>
        </div>
    </div>
    <div class="clr"></div>
</div>
<div class="pageContainer" id="our-team">
    <div class="pageContainerTop"></div>
    <div class="pageContentArea">
        <h1 class="pageTitle">OUR TEAM</h1>

        <div class="realContent">
            <div class="teamCon" id="our-team-slide">
                <ul class="bjqs">
                    <?php $x = 0; foreach ($teamList as $team) {$x++?>
                    <?php if ($x == 1){?> <li> <?php }?>

                        <div class="teamBox">
                            <div class="teamImg"><img src="<?=base_url("photos/".$team->t05)?>" width="215" height="215" alt="ornek"/>
                            </div>
                            <div class="teamName"><?=$team->t01?></div>
                            <div class="teamTitle"><?=$team->t02?></div>
                        </div>

                    <?php if ($x == 4){$x=0?> </li> <?php }?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="pageContainer" id="career">
    <div class="pageContainerTop"></div>
    <div class="pageContentArea">
        <h1 class="pageTitle">CAREER</h1>

        <div class="realContent">
            <h2><?=$workWithUs->t01?></h2>
            <?=$workWithUs->t02?>
            <a class="sendMail" href="mailto:cv@linkmice.com" title="cv@linkmice.com">cv@linkmice.com</a>
            <div class="clr"></div>
        </div>
    </div>
</div>
<div class="pageContainer" id="references">
    <div class="pageContainerTop"></div>
    <div class="pageContentArea">
        <h1 class="pageTitle">REFERENCES</h1>

        <div class="realContent">
            <div class="referenceCon">
                <?php foreach($references as $reference) { ?>
                <a href="javascript:;" title="" target="_blank"><img src="<?=base_url("photos/".$reference->t01)?>" width="230"
                                                                     height="128" alt="Ornek"/></a>
                <?php }?>
            </div>
        </div>
    </div>
</div>
<div class="pageContainer" id="contact">
    <div class="pageContainerTop"></div>
    <div class="pageContentArea">
        <h1 class="pageTitle">CONTACT</h1>

        <div class="realContent">
            <div class="googleMap">
                <iframe width="960" height="274" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                        src="<?=base_url("map.html")?>"></iframe>
                <!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3008.057305637268!2d29.016451!3d41.06774!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab645fb9fd1a5%3A0x272558825cc8e0dd!2sZorlu+Center+PSM!5e0!3m2!1str!2s!4v1415798267706" width="960" height="274" frameborder="0" style="border:0"></iframe>-->
            </div>
            <div class="contactBox">
                <h3>CONTACT INFORMATIONS</h3>
                <span>LinkMice</span>

                <p><a href="mailto:linkmice@linkmice.com" title="linkmice@linkmice.com">linkmice@linkmice.com</a></p>
                <p>
                    Etiler Mh. Bıyıklı Mehmet Paşa Sk.<br> No:11/2 Çamlık Apt. <br>Beşiktaş/İstanbul<br>Tel: +90 2122632001

                </p>

            </div>
            <div class="contactBox">
                <h3>CONTACT FORM</h3>

                <form id="contactForm" name="contactForm" action="">
                    <input type="text" id="name" name="name" placeholder="Name, Surname" required="required"/>
                    <input type="mail" id="email" name="email" placeholder="E-Mail Address" required="required"/>
                    <input type="tel" id="phone" name="phone" placeholder="Phone Number" required="required"/>
                    <input type="text" id="subject" name="subject" placeholder="Subject" required="required"/>
                    <textarea placeholder="Message" required="required"></textarea>
                    <input type="submit" value="Send" class="sendForm"/>
                </form>
            </div>
        </div>
        <div class="clr"></div>
    </div>
</div>
</div>
<div class="footerContainer">
    <div class="footerContent">
        <div class="footerTop">
            <span>Newsletters can sign up for our E-Mail Group.</span>

            <form id="" name="" action="">
                <input type="email" id="newsletter" name="newsletter" placeholder="E-Mail Address" required="required"/>
                <input type="submit" id="" name="" value="Send" class="sendMail"/>
            </form>
        </div>
        <ul class="footerLogos">
            <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/1.jpg" alt=""/></a></li>
            <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/2.jpg" alt=""/></a></li>
            <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/3.jpg" alt=""/></a></li>
            <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/4.jpg" alt=""/></a></li>
            <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/5.jpg" alt=""/></a></li>
            <li><a href="javascript:;" title="" target="_blank"><img src="images/logos/6.jpg" alt=""/></a></li>
        </ul>
    </div>
</div>

</div>
<script src="script/jquery.mCustomScrollbar.concat.min.js"></script>
<script class="secret-source">
    jQuery(document).ready(function ($) {
        $('#our-team-slide').bjqs({
            height: 320,
            width: 940,
            animtype: 'slide',
            automatic: false,
            responsive: false,
            randomstart: true
        });
    });
</script>
<script type="text/javascript">
    var revapi;
    jQuery(document).ready(function () {
        "use strict";
        revapi = jQuery('.fullwidthbanner').revolution({
            dottedOverlay: "linebottom",
            delay: 4000,
            startwidth: 1140,
            startheight: 450,
            hideThumbs: 200,

            thumbWidth: 100,
            thumbHeight: 50,
            thumbAmount: 3,

            navigationType: "none",
            navigationArrows: "solo",
            navigationStyle: "round",

            touchenabled: "on",
            onHoverStop: "off",

            navigationHAlign: "center",
            navigationVAlign: "bottom",
            navigationHOffset: 0,
            navigationVOffset: 20,

            soloArrowLeftHalign: "left",
            soloArrowLeftValign: "center",
            soloArrowLeftHOffset: 20,
            soloArrowLeftVOffset: 0,

            soloArrowRightHalign: "right",
            soloArrowRightValign: "center",
            soloArrowRightHOffset: 20,
            soloArrowRightVOffset: 0,

            shadow: 0,
            fullWidth: "on",
            fullScreen: "on",
            lazyLoad: "on",

            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,

            shuffle: "off",

            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            startWithSlide: 0,
        });
    });
</script>
</body>
</html>
