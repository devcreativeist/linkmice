  <form class="m-b-none" method="post" action="<?=base_url("admin/anasayfakaydet")?>" enctype="multipart/form-data">
  <input type="hidden" name="id" id="id" value="<?=$sayfa->id?>">
  <div class="modal-dialog">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
  <h4 class="modal-title" id="myModalLabel">Anasayfa</h4>
  </div>
  <div class="modal-body">         
  <div class="block"><div class="col-lg-3"><img src="<?=base_url("photos/".$sayfa->resim)?>" id="resim2" height="50"></div><div class="col-lg-9"><input type="file" class="form-control"  name="userfile">1920x1020 Pixel</div></div>
  <br><hr><br>
  <div class="block"><label class="control-label">İsim</label><input type="text" class="form-control"  name="isim" id="isim" value="<?=$sayfa->isim?>"></div>
  <div class="block"><label class="control-label">Mekan</label><input type="text" class="form-control"  name="mekan" id="mekan" value="<?=$sayfa->mekan?>"></div>                    
  <div class="block"><label class="control-label">Açıklama</label><textarea class="form-control"  name="aciklama" id="aciklama"><?=$sayfa->aciklama?></textarea></div>
  <div class="block"><label class="control-label">URL</label><input type="text" class="form-control"  name="url" id="url" value="<?=$sayfa->url?>"></div>
  <div class="block"><label class="control-label">Kategori</label>
  <select class="form-control" name="kategori">
  <option value="">Seçiniz</option>
	<?php
  foreach ($this->db->get('kategoriler')->result() as $k) {?>
  <option value="<?=$k->id?>" <?php if ($k->id==$sayfa->kategori) {echo "selected";} ?>><?=$k->t01?></option>
  <?php }?>
  </select>
  </div>
  </div>
  <div class="modal-footer"><button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button><button type="submit" class="btn btn-sm btn-primary">Kaydet</button></div>
  </div><!-- /.modal-content -->
  </div>
  </form>