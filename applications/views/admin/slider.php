  <section id="content">
    <section class="main padder">
			<div class="clearfix"><h4><i class="icon-table"></i><?=$sayfa->t01?></h4></div>      
      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
						<header class="panel-heading">SLİDER LİSTESİ</header>
            <div class="panel-body">
            	<div style="padding:25px;">
              	<style>
								ul.galerinova li {float:left;list-style:none;margin:5px;}
								</style>
                <ul class="galerinova">
                	<?php	foreach($galeri->result() as $r) {?>                	
                  <li><img src="<?=base_url("photos/".$r->t01)?>"><br><a href="<?=base_url("admin/slidersil/slider/".$r->id.'/'.$sayfa->id)?>"><i class="icon-trash" ></i></a></li>
                  <?php } ?>
                </ul>
							</div>
            </div>            
            <footer class="panel-footer">
            <div class="row">
              <div class="col-sm-4 hidden-xs"><a class="btn btn-sm btn-info" data-toggle="modal" href="#ekle"><i class="icon-plus"></i> EKLE</a></div>
              <div class="col-sm-3 text-center"></div>
              <div class="col-sm-5 text-right text-center-sm"></div>
            </div>
            </footer>
          </section>
          <div id="ekle" class="modal fade">
            <form class="m-b-none" method="post" action="<?=base_url("admin/sliderkaydet")?>" enctype="multipart/form-data">
            	<input type="hidden" name="callback" value="<?=$id?>">
              <input type="hidden" name="id" value="<?=$id?>">
              <input type="hidden" name="tbl" value="kategori">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
                    <h4 class="modal-title" id="myModalLabel">Resim Ekle</h4>
                  </div>
                  <div class="modal-body">                             
                    <div class="block" style="margin:15px;"><label class="control-label">Resim</label><input type="file" class="form-control"  name="userfile"></div>
                    <div class="block" style="margin:15px;"><label class="control-label">Satır 1</label><input type="text" class="form-control"  name="t02"></div>
                    <div class="block" style="margin:15px;"><label class="control-label">Satır 2</label><input type="text" class="form-control"  name="t03"></div>
                    <div class="block" style="margin:15px;"><label class="control-label">Ürün Kategorisi</label>
                    	<select class="form-control" name="kategori">
                      	<?php
												foreach ($this->db->get('kategoriler')->result() as $k) { ?>
                        <option value="<?=$k->id?>"><?=$k->t01?></option>
												<?php } ?>
                    </select>
                   </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button>
                    <button type="submit" class="btn btn-sm btn-primary">Kaydet</button>
                  </div>
                </div><!-- /.modal-content -->
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </section>
  