  <section id="content">
    <section class="main padder">
			<div class="clearfix"><h4><i class="icon-table"></i><?=$sayfa->t01?></h4></div>      
      <div class="row">
        <div class="col-lg-12">
          <section class="panel">
						<header class="panel-heading">DÖKÜMAN LİSTESİ</header>
              <div class="table-responsive">
                <table class="table table-striped b-t text-small">
                  <thead>
                    <tr>
                      <th data-toggle="class">TÜR</th>
                      <th data-toggle="class">DÖKÜMAN</th>
                      <th>İŞLEM</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    foreach($pdf->result() as $u){
                    ?>
                    <tr>
                      <td><?=$u->t01?></td>
                      <td><?=$u->t02?></td>
                      <td>
                        <!--<a href="#duzenle" rel="<?=$u->id?>"  data-toggle="modalhaberedit" class="cek" alt="kategoriler"><i class="icon-pencil" title="Düzenle"></i></a>&nbsp;&nbsp;-->
                        <a href="<?=base_url("admin/teknikdokumansil/pdf/".$u->id.'/'.$sayfa->id)?>"><i class="icon-trash" title="Sil"></i></a>
                      </td>
                    </tr>
                    <?php }?>
                  </tbody>
                </table>
              </div>
            <footer class="panel-footer">
            <div class="row">
              <div class="col-sm-4 hidden-xs"><a class="btn btn-sm btn-info" data-toggle="modal" href="#ekle"><i class="icon-plus"></i> EKLE</a></div>
              <div class="col-sm-3 text-center"></div>
              <div class="col-sm-5 text-right text-center-sm"></div>
            </div>
            </footer>
          </section>
          <div id="ekle" class="modal fade">
            <form class="m-b-none" method="post" action="<?=base_url("admin/teknikdokumankaydet")?>" enctype="multipart/form-data">
            	<input type="hidden" name="callback" value="<?=$id?>">
              <input type="hidden" name="bid" value="<?=$id?>">
              <input type="hidden" name="tbl" value="teknik">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
                    <h4 class="modal-title" id="myModalLabel">Resim Ekle</h4>
                  </div>
                  <div class="modal-body">                             
                    <div class="block"><label>Tür</label><input type="text" class="form-control" name="t01"></div>
                    <div class="block"><label>Döküman</label><input type="text" class="form-control" name="t02"></div>
                    <div class="block"><label>Dosya</label><input type="text" class="form-control"name="t03"></div>                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button>
                    <button type="submit" class="btn btn-sm btn-primary">Kaydet</button>
                  </div>
                </div><!-- /.modal-content -->
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </section>
  