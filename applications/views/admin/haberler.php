  <section id="content">
    <section class="main padder">
			<div class="clearfix"><h4><i class="icon-home"></i>NEWS</h4></div>

          <section class="panel">
            <header class="panel-heading">
              Liste
            </header>
            <div class="panel-body"><a class="btn btn-sm btn-info" data-toggle="modal" href="#ekle"><i class="icon-plus"></i> EKLE</a></div>
            <div class="table-responsive">
              <table class="table table-striped b-t text-small">
                <thead>
                  <tr>
                    <th data-toggle="class">BAŞLIK</th>
                    <th>İŞLEM</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($haberler->result() as $u){?>
                  <tr>
                    <td><?=$u->t01?></td>
                    <td>
                    	<a href="#duzenle" rel="<?=$u->id?>"  data-toggle="modalhaberedit" class="cek" alt="haberler" ><i class="icon-pencil"></i></a>&nbsp;&nbsp;
                      <a href="<?=base_url("admin/habersil/haberler/".$u->id)?>"><i class="icon-trash" ></i></a>                      
                    </td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
            <footer class="panel-footer"></footer>
          </section>

      <div id="ekle" class="modal fade">
        <form class="m-b-none" method="post" action="<?=base_url("admin/haberkaydet")?>" enctype="multipart/form-data">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
                <h4 class="modal-title" id="myModalLabel">Ekle</h4>
              </div>
              <div class="modal-body">
                  <div class="block"><label class="control-label">Resim</label><input type="file" class="form-control"  name="userfile">220x220 Pixel</div>
                  <div class="block"><label class="control-label">Başlık</label><input type="text" class="form-control"  name="t01"></div>
                <div class="block"><label class="control-label">Tarih</label><input type="text" class="form-control"  name="t02"></div>
                <div class="block"><label class="control-label">Açıklama</label><textarea class="form-control"  name="t03"></textarea></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button>
                <button type="submit" class="btn btn-sm btn-primary">Kaydet</button>
              </div>
            </div><!-- /.modal-content -->
          </div>
        </form>
      </div>
      <div id="duzenle" class="modal fade">
        <form class="m-b-none" method="post" action="<?=base_url("admin/haberkaydet")?>" enctype="multipart/form-data">
          <input type="hidden" name="id" id="id" value="">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
                <h4 class="modal-title" id="myModalLabel">Düzenle</h4>
              </div>
              <div class="modal-body">
                <div class="block"><label class="control-label">Resim</label><input type="file" class="form-control"  name="userfile">220x220 Pixel</div>
                <div class="block"><label class="control-label">Başlık</label><input type="text" class="form-control"  name="t01" id="t01"></div>
                <div class="block"><label class="control-label">Tarih</label><input type="text" class="form-control"  name="t02" id="t02"></div>                    
                <div class="block"><label class="control-label">Açıklama</label><textarea class="form-control"  name="t03" id="t03"></textarea></div>
              </div>
              <div class="modal-footer"><button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button><button type="submit" class="btn btn-sm btn-primary">Kaydet</button></div>
            </div><!-- /.modal-content -->
          </div>
        </form>
      </div>
    </section>
  </section>