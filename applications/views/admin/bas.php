<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>LinkMice | Admin</title>
  <meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <link rel="stylesheet" href="<?=base_url("admin-resources/css/bootstrap.css")?>">
  <link rel="stylesheet" href="<?=base_url("admin-resources/css/font-awesome.min.css")?>">
  <link rel="stylesheet" href="<?=base_url("admin-resources/css/font.css")?>" cache="false">
	<link rel="stylesheet" href="<?=base_url("admin-resources/css/style.css")?>">
  <link rel="stylesheet" href="<?=base_url("admin-resources/css/plugin.css")?>">
  <link rel="stylesheet" href="<?=base_url("admin-resources/css/landing.css")?>">
  <script src="<?=base_url("admin-resources/js/jquery.min.js")?>"></script>
  <script src="http://malsup.github.com/jquery.form.js"></script>
  <!--[if lt IE 9]>
    <script src="<?=base_url("admin-resources/js/ie/respond.min.js")?>" cache="false"></script>
    <script src="<?=base_url("admin-resources/js/ie/html5.js")?>" cache="false"></script>
  <![endif]-->
</head>
<body>
  <!-- header -->
	<header id="header" class="navbar">
    <ul class="nav navbar-nav navbar-avatar pull-right">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">            
          <span class="hidden-xs-only">Link Mice</span>
          <span class="thumb-small avatar inline"><img src="<?=base_url("admin-resources/images/avatar.png")?>" class="img-circle"></span>
          <b class="caret hidden-xs-only"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="<?=base_url("admin/logout")?>">Çıkış</a></li>
        </ul>
      </li>
    </ul>
    <a class="navbar-brand" href="#" style="">&nbsp; <img src="<?=base_url("admin-resources/images/logo.png")?>" height="31"> &nbsp;</a>
    <button type="button" class="btn btn-link pull-left nav-toggle visible-xs" data-toggle="class:slide-nav slide-nav-left" data-target="body">
      <i class="icon-reorder icon-xlarge text-default"></i>
    </button>
    <ul class="nav navbar-nav hidden-xs">
      <li class="dropdown shift" data-toggle="shift:appendTo" data-target=".nav-primary .nav">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog icon-xlarge visible-xs visible-xs-inline"></i>Ayarlar <b class="caret hidden-sm-only"></b></a>
        <ul class="dropdown-menu">
          <li>
            <a href="#" data-toggle="class:navbar-fixed" data-target='body'>Navigasyon
              <span class="text-active">Otomatik</span>
              <span class="text">Sabit</span>
            </a>
          </li>
          <li class="hidden-xs">
            <a href="#" data-toggle="class:nav-vertical" data-target="#nav">Navigasyon 
              <span class="text">dikey</span>
              <span class="text-active">yatay</span>
            </a>
          </li>
        </ul>
      </li>
    </ul>
	</header>
  <!-- / header -->
  <!-- nav -->
  <nav id="nav" class="nav-primary hidden-xs nav-horizontal">
    <ul class="nav" data-spy="affix" data-offset-top="60" style="margin-top:12px">
        <li><a href="<?=base_url("admin")?>"><span>Welcome Slider</span></a></li>
        <li><a href="<?=base_url("admin/hakkimizda")?>"><span>About US</span></a></li>
        <li><a href="<?=base_url("admin/amacimiz")?>"><span>Career</span></a></li>
        <li><a href="<?=base_url("admin/bizkimiz")?>"><span>Team</span></a></li>
        <li><a href="<?=base_url("admin/haberler")?>"><span>News</span></a></li>
        <li><a href="<?=base_url("admin/referanslar")?>"><span>What We do</span></a></li>
        <li><a href="<?=base_url("admin/galeriresimler/1")?>"><span>Photo Gallery</span></a></li>
        <li><a href="<?=base_url("admin/galeriresimler/2")?>"><span>Video Gallery</span></a></li>
        <li><a href="<?=base_url("admin/galeriresimler/3")?>"><span>References</span></a></li>
    </ul>
  </nav>
  <!-- / nav -->