  <section id="content">
    <section class="main padder">
    	<div class="clearfix"><h4><i class="icon-home"></i>BİZ KİMİZ</h4></div>
          <section class="panel">
						<header class="panel-heading">Liste</header>
            <div class="panel-body"><div class="row text-small"><div class="col-sm-4 m-b-mini"><a class="btn btn-sm btn-info" data-toggle="modal" href="#ekle"><i class="icon-plus"></i> EKLE</a></div></div></div>
            <div class="table-responsive">
              <table class="table table-striped b-t text-small">
                <thead>
                  <tr>
                    <th data-toggle="class">AD SOYAD</th>
                    <th data-toggle="class">GÖREVİ</th>

                    <th>İŞLEM</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($ekip->result() as $u){?>
                  <tr>
                    <td><?=$u->t01?></td>
                    <td><?=$u->t02?></td>

                    <td>
                    	<div id="edit<?=$u->id?>" class="modal fade"></div>
                      <a href="#edit<?=$u->id?>" rel="<?=$u->id?>"  
                      data-toggle="modaledit" 
                      data-remote="<?=base_url("admin/bizkimizedit/".$u->id)?>"
                      alt="sss"><i class='icon-pencil'></i></a>&nbsp;&nbsp;&nbsp;
                      <a href="<?=base_url("admin/ekipsil/ekip/".$u->id)?>"><i class="icon-trash" ></i></a>                      
                    </td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
            <footer class="panel-footer"></footer>
          </section>

      <div id="ekle" class="modal fade">
        <form class="m-b-none" method="post" action="<?=base_url("admin/ekipkaydet")?>" enctype="multipart/form-data">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
                <h4 class="modal-title" id="myModalLabel">Biz Kimiz</h4>
              </div>
              <div class="modal-body">         
                <div class="block"><label class="control-label">Resim</label><input type="file" class="form-control"  name="userfile">215x215 Pixel</div>
                <div class="block"><label class="control-label">Ad Soyad</label><input type="text" class="form-control"  name="t01"></div>
                <div class="block"><label class="control-label">Görevi</label><input type="text" class="form-control"  name="t02"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button>
                <button type="submit" class="btn btn-sm btn-primary">Kaydet</button>
              </div>
            </div><!-- /.modal-content -->
          </div>
        </form>
      </div>                                    
    </section>
  </section>