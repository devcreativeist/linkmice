  <section id="content">
    <section class="main padder">
      <div class="clearfix"><h4><i class="icon-home"></i>ANASAYFA</h4></div>      
      <ul class="nav nav-tabs m-b-none no-radius">
        <li class="active"><a href="#icerik" data-toggle="tab">İÇERİK</a></li>
        <li><a href="#seo" data-toggle="tab">SEO</a></li>
	    </ul>
			<div class="tab-content">
      	<div class="tab-pane active" id="icerik">
          <section class="panel">
            <header class="panel-heading">Anasayfa Slider Liste</header>
            <div class="panel-body"><div class="row text-small"><div class="col-sm-4 m-b-mini"><a class="btn btn-sm btn-info" data-toggle="modal" href="#ekle"><i class="icon-plus"></i> EKLE</a></div></div></div>
            <div class="table-responsive">
              <table class="table table-striped b-t text-small">
				<thead><tr><th data-toggle="class">İSİM</th><th>İŞLEM</th></tr></thead>
                <tbody>
                  <?php foreach($anasayfa->result() as $u){?>
                  <tr>
                    <td><?=$u->isim?></td>
                    <td>
                    	<div id="edit<?=$u->id?>" class="modal fade"></div>
                      <a href="#edit<?=$u->id?>" rel="<?=$u->id?>"  
                      data-toggle="modaledit" 
                      data-remote="<?=base_url("admin/anasayfaedit/".$u->id)?>"
                      alt="sss"><i class='icon-pencil'></i></a>&nbsp;&nbsp;&nbsp;
                      <a href="<?=base_url("admin/anasayfasil/anasayfa/".$u->id)?>"><i class="icon-trash" ></i></a>                      
                    </td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
            <footer class="panel-footer">
            </footer>
          </section>
        </div>
        <div class="tab-pane" id="seo">
        	<form method="post" action="<?=base_url("admin/seo")?>" enctype="multipart/form-data">
          <input type="hidden" name="id" value="8">
          <input type="hidden" name="callback"  value="">
          <input type="hidden" name="tbl" value="sayfa">
          <section class="panel"> 
            <div class="block" style="margin:15px;"><label class="control-label">Sayfa Başlığı</label><input type="text" class="form-control"  name="title" id="title" value="<?=$sayfa->title?>"></div>
            <div class="block" style="margin:15px;"><label class="control-label">Anahtar Kelimeler</label><input type="text" class="form-control"  name="keys" id="keys" value="<?=$sayfa->keys?>"></div>                    
            <div class="block" style="margin:15px;"><label class="control-label">Açıklama</label><textarea class="form-control"  name="desc" id="desc"><?=$sayfa->desc?></textarea></div>
            <footer class="panel-footer"><div class="row"><div class="col-sm-4 hidden-xs"><button type="submit" class="btn btn-sm btn-primary">Kaydet</button></div></div></footer>
          </section>
        </form>
	      </div>
        <div id="ekle" class="modal fade">
          <form class="m-b-none" method="post" action="<?=base_url("admin/anasayfakaydet")?>" enctype="multipart/form-data">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
                  <h4 class="modal-title" id="myModalLabel">Anasayfa</h4>
                </div>
                <div class="modal-body">         
                  <div class="block"><label class="control-label">İsim</label><input type="text" class="form-control"  name="isim"></div>
                  <div class="block"><label class="control-label">Resim</label><input type="file" class="form-control"  name="userfile">1920x1020 Pixel</div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button>
                  <button type="submit" class="btn btn-sm btn-primary">Kaydet</button>
                </div>
              </div><!-- /.modal-content -->
            </div>
          </form>
        </div>
      </div>
    </section>
  </section>