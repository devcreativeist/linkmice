  <section id="content">
    <section class="main padder">
			<div class="clearfix"><h4><i class="icon-home"></i>GALERİ</h4></div> 
      <section class="panel">
        <header class="panel-heading">Liste</header>
        <div class="panel-body"><a class="btn btn-sm btn-info" data-toggle="modal" href="#ekle"><i class="icon-plus"></i>EKLE</a></div>
        <div class="table-responsive">
          <table class="table table-striped b-t text-small">
            <thead>
              <tr>
                <th data-toggle="class">BAŞLIK</th>
                <th>İŞLEM</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              foreach($sayfa->result() as $u){
              ?>
              <tr>
                <td><?=$u->t01?></td>
                <td>
                  <!--<a href="#duzenle" rel="<?=$u->id?>"  data-toggle="modalhaberedit" class="cek" alt="kategoriler"><i class="icon-pencil" title="Düzenle"></i></a>&nbsp;&nbsp;-->
                  <a href="<?=base_url("admin/galeriresimler/".$u->id)?>" rel="<?=$u->id?>"  data-toggle="" class="cek" alt="kategoriler" ><i class="icon-picture" title="Galeri"></i></a>&nbsp;&nbsp;
                  <a href="<?=base_url("admin/galerisil/galeri/".$u->id)?>"><i class="icon-trash" title="Sil"></i></a>
                </td>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
        <footer class="panel-footer"></footer>
      </section>
      <div id="ekle" class="modal fade">
        <form class="m-b-none" method="post" action="<?=base_url("admin/galerikaydet")?>" enctype="multipart/form-data">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
                <h4 class="modal-title" id="myModalLabel">Ekle</h4>
              </div>
              <div class="modal-body">         
                <div class="block"><label class="control-label">Başlık</label><input type="text" class="form-control"  name="t01"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button>
                <button type="submit" class="btn btn-sm btn-primary">Kaydet</button>
              </div>
            </div>
          </div>
        </form>
      </div>            
    </section>
  </section>