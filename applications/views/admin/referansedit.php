        <form class="m-b-none" method="post" action="<?=base_url("admin/referanskaydet")?>" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?=$sayfa->id?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
                <h4 class="modal-title" id="myModalLabel">Ekle</h4>
              </div>
              <div class="modal-body">         
                <div class="block"><label class="control-label">Başlık</label><input type="text" class="form-control"  name="t01" value="<?=$sayfa->t01?>"></div>
                <div class="block"><label class="control-label">Açıklama</label><textarea class="form-control"  name="t03"><?=$sayfa->t03?></textarea></div>
                <div class="block">
                  <img src="<?=base_url("photos/".$sayfa->t04)?>" height="50">
                  <label class="control-label">Büyük Resim</label><input type="file" class="form-control"  name="buyuk">
                </div>
                <div class="block">
                  <img src="<?=base_url("photos/".$sayfa->t05)?>" height="50">
                  <label class="control-label">Küçük Resim</label><input type="file" class="form-control"  name="kucuk">
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button>
                <button type="submit" class="btn btn-sm btn-primary">Kaydet</button>
              </div>
            </div><!-- /.modal-content -->
          </div>
        </form>