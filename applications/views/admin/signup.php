<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Link Mice | Admin</title>
  <meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
  <link rel="stylesheet" href="./admin-resources/css/bootstrap.css">
  <link rel="stylesheet" href="./admin-resources/css/font-awesome.min.css">
  <link rel="stylesheet" href="./admin-resources/css/font.css" cache="false">
  <link rel="stylesheet" href="./admin-resources/css/style.css">
  <!--[if lt IE 9]>
    <script src="./admin-resources/js/ie/respond.min.js" cache="false"></script>
    <script src="./admin-resources/js/ie/html5.js" cache="false"></script>
  <![endif]-->
</head>
<body>
  <!-- header -->
  <header id="header" class="navbar bg bg-black">
    <a href="./admin-resources/docs.html" class="btn btn-link pull-right m-t-mini"><i class="icon-question icon-xlarge text-default"></i></a>
    <a class="navbar-brand" href="#"><img src="<?=base_url("admin-resources/images/logo.png")?>" height="40" style="margin-left:20px"></a>
  </header>
  <!-- / header -->
  <section id="content">
    <div class="main padder">
      <div class="row">
        <div class="col-lg-4 col-lg-offset-4 m-t-large">
          <section class="panel">
            <header class="panel-heading text-center">
              İçerik Yönetim Paneli
            </header>
            <form method="post" action="<?php echo base_url("signup/process");?>" class="panel-body">
              <div class="block">
                <label class="control-label">Kullanıcı Adı</label>
                <input type="email" placeholder="test@ornek.com" name="username" class="form-control">
              </div>
              <div class="block">
                <label class="control-label">Şifre</label>
                <input type="password" id="inputPassword" placeholder="Şifre" name="password" class="form-control">
              </div>
              <button type="submit" class="btn btn-info">Giriş</button>
              <div class="line line-dashed"></div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </section>
  <!-- footer -->
  <footer id="footer">
  </footer>
  <!-- / footer -->
	<script src="./admin-resources/js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="./admin-resources/js/bootstrap.js"></script>
  <!-- app -->
  <script src="./admin-resources/js/app.js"></script>
  <script src="./admin-resources/js/app.plugin.js"></script>
  <script src="./admin-resources/js/app.data.js"></script>
</body>
</html>