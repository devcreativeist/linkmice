<!-- footer -->
  <footer id="footer">
  </footer>
  <script src="<?=base_url("admin-resources/js/bootstrap.js")?>"></script>
  <script src="<?=base_url("admin-resources/js/fuelux/fuelux.js")?>"></script>
  <script src="<?=base_url("admin-resources/js/underscore-min.js")?>"></script>
  <script src="<?=base_url("admin-resources/js/file-input/bootstrap.file-input.js")?>"></script>
  <script src="<?=base_url("admin-resources/js/charts/sparkline/jquery.sparkline.min.js")?>"></script>
  <script src="<?=base_url("admin-resources/js/charts/easypiechart/jquery.easy-pie-chart.js")?>"></script>
  <script src="<?=base_url("admin-resources/js/app.js")?>"></script>
  <script src="<?=base_url("admin-resources/js/app.plugin.js")?>"></script>
  <script src="<?=base_url("admin-resources/js/tinymce/tinymce.min.js")?>"></script>
	<script>tinymce.init({selector: "textarea.editor",language : 'tr_TR',});</script>
</body>
</html>