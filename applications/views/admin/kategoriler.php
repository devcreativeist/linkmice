  <section id="content">
    <section class="main padder">
			<div class="clearfix"><h4><i class="icon-home"></i>KATEGORİLER</h4></div> 
      <section class="panel">
        <header class="panel-heading">Liste</header>
        <div class="panel-body"><a class="btn btn-sm btn-info" data-toggle="modal" href="#ekle"><i class="icon-plus"></i> EKLE</a></div>
        <div class="table-responsive">
          <table class="table table-striped b-t text-small">
            <thead>
              <tr>
                <th data-toggle="class">BAŞLIK</th>
                <th>İŞLEM</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              foreach($kategoriler->result() as $u){
              ?>
              <tr>
                <td><?=$u->t01?></td>
                <td>
                  <a href="#duzenle" rel="<?=$u->id?>"  data-toggle="modalhaberedit" class="cek" alt="kategoriler"><i class="icon-pencil" title="Düzenle"></i></a>&nbsp;&nbsp;
                  <a href="#seo" rel="<?=$u->id?>"  data-toggle="modalseo" class="cek" alt="kategoriler" ><i class="icon-cogs" title="Seo"></i></a>&nbsp;&nbsp;
                  <a href="<?=base_url("admin/pdf/".$u->id)?>" rel="<?=$u->id?>"  data-toggle="modalpdf" class="cek" alt="kategoriler" ><i class="icon-file" title="Katalog"></i></a>&nbsp;&nbsp;
                  <a href="<?=base_url("admin/kategorigaleri/".$u->id)?>" rel="<?=$u->id?>"  data-toggle="" class="cek" alt="kategoriler" ><i class="icon-picture" title="Galeri"></i></a>&nbsp;&nbsp;
                  <a href="<?=base_url("admin/slider/".$u->id)?>"  data-toggle="" class="cek" alt="kategoriler" ><i class="icon-expand" title="Slider"></i></a>&nbsp;&nbsp;                    
                  <a href="<?=base_url("admin/kategorisil/kategoriler/".$u->id)?>"><i class="icon-trash" title="Sil"></i></a>
                </td>
              </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
        <footer class="panel-footer"></footer>
      </section>
      <div id="ekle" class="modal fade">
        <form class="m-b-none" method="post" action="<?=base_url("admin/kategorikaydet")?>" enctype="multipart/form-data">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
                <h4 class="modal-title" id="myModalLabel">Ekle</h4>
              </div>
              <div class="modal-body">         
                <div class="block"><label class="control-label">Başlık</label><input type="text" class="form-control"  name="t01"></div>
                <div class="block"><label class="control-label">Açıklama</label><textarea class="form-control"  name="t03"></textarea></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button>
                <button type="submit" class="btn btn-sm btn-primary">Kaydet</button>
              </div>
            </div><!-- /.modal-content -->
          </div>
        </form>
      </div>
      <div id="duzenle" class="modal fade">
        <form class="m-b-none" method="post" action="<?=base_url("admin/kategorikaydet")?>" enctype="multipart/form-data">
          <input type="hidden" name="id" id="id" value="">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
                <h4 class="modal-title" id="myModalLabel">Düzenle</h4>
              </div>
              <div class="modal-body">         
                <div class="block"><label class="control-label">Başlık</label><input type="text" class="form-control"  name="t01" id="t01"></div>
                <div class="block"><label class="control-label">Açıklama</label><textarea class="form-control"  name="t03" id="t03"></textarea></div>
              </div>
              <div class="modal-footer"><button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button><button type="submit" class="btn btn-sm btn-primary">Kaydet</button></div>
            </div><!-- /.modal-content -->
          </div>
        </form>
      </div>
      <div id="seo" class="modal fade">
          <form class="m-b-none" method="post" action="<?=base_url("admin/seo")?>" enctype="multipart/form-data">
            <input type="hidden" name="id" id="id2" value="">
            <input type="hidden" name="callback" id="callback" value="kategoriler">
            <input type="hidden" name="tbl" id="tbl" value="kategoriler">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
                  <h4 class="modal-title" id="myModalLabel">SEO</h4>
                </div>
                <div class="modal-body">         
                  <div class="block"><label class="control-label">Başlık</label><input type="text" class="form-control"  name="title" id="title"></div>
                  <div class="block"><label class="control-label">Anahtar Kelimeler</label><input type="text" class="form-control"  name="keys" id="keys"></div>
                  <div class="block"><label class="control-label">Açıklama</label><textarea class="form-control"  name="desc" id="desc"></textarea></div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Vazgeç</button>
                  <button type="submit" class="btn btn-sm btn-primary">Kaydet</button>
                </div>
              </div><!-- /.modal-content -->
            </div>
          </form>
        </div>
    </section>
  </section>